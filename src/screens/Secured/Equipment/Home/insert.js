import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Modal, Switch } from 'react-native';
import { Container, Content, Form, List, View, Icon, Picker, Input, Label, Item, Button, Text, CheckBox, Body, ListItem, Title } from 'native-base';
import { handleErrorResponse } from '../../../../service/error_handler';
import { changeHeaderTitle, changeSideBarInivisble } from '../../../../actions/shared';
import NavigationService from '../../../../service/NavigationService';
import apiStatus from '../../../../api/status';
import apiteams from '../../../../api/teams';
import apicategory from '../../../../api/categories';
import apimodels from '../../../../api/models';
import BaseComponent from '../../BaseComponent';
import apiequipment from '../../../../api/equipment';


class InsertEquipment extends BaseComponent {


	constructor(props) {
		super(props);
		this.state = {
			teams: [{
				id: 0,
				name: "--select--"
			}],

			statuses: [{
				id: 0,
				name: "--select--"
			}],

			categories: [{
				id: 0,
				name: "--select--"
			}],

			models: [{
				id: 0,
				name: "--select--"
			}],

			company_id: 0,
			newcategory: "",
			newcategoryEnabled: true,
			newprefix: "",
			newprefixEnabled: true,
			newmodel: "",
			newmodelEnabled: true,
			quantity: "1",
			automatic: true,
			selectedCategory: 0,
			selectedModel: 0,
			selectedStatus: 0,
			selectedTeam: 0,
			visibleModal: false,
			equipmentserials: []
		};
		this.refreshData = this.refreshData.bind(this);
		this.refreshModel = this.refreshModel.bind(this);
		this.showModal = this.showModal.bind(this);
		this.dismissModal = this.dismissModal.bind(this);
		this.postequipmentBody = this.postequipmentBody.bind(this);
	}

	postequipmentBody() {
		const {newcategoryEnabled, newmodelEnabled} = this.state;
		if (newcategoryEnabled && newmodelEnabled) {
			const categoryBody = {
				name: this.state.newcategory,
				prefix: this.state.newprefix,
				company_id: this.state.company_id
			};
			apicategory.store(categoryBody).then(response => {
				const {category} = response.data;
				const modelBody = {
					name: this.state.newmodel,
					category_id: category.id,
					company_id: this.state.company_id
				};
				console.log(JSON.stringify(modelBody, null, 3));
				return apimodels.store(modelBody)
			}).then(response => {
				const {model} = response.data;
				console.log("Model",  JSON.stringify(model, null, 3));
				let requestBody = (this.state.automatic) ? {
					model_id: model.id,
					category_id: model.category_id,
					team_id: this.state.selectedTeam,
					status_id: this.state.selectedStatus,
					quantity: 1,
					auto_assign: "yes",
					company_id: this.state.company_id
				}: {
					model_id: model.id,
					category_id: model.category_id,
					team_id: this.state.selectedTeam,
					status_id: this.state.selectedStatus,
					quantity: this.state.quantity,
					auto_assign: "no",
					serials: this.state.equipmentserials,
					company_id: this.state.company_id	
				}
				console.log(JSON.stringify(requestBody, null, 3));
				return apiequipment.store(requestBody)
			}).then(response => {
				this.props.navigation.goBack();				
			}).catch(err => {
				console.log(JSON.stringify(err, null, 3));
			})
		} else if (newcategoryEnabled && !newmodelEnabled) {
			const categoryBody = {
				name: this.state.newcategory,
				prefix: this.state.newprefix,
				company_id: this.state.company_id
			};
			console.log("Category body ", JSON.stringify(categoryBody, null, 3));
			apicategory.store(categoryBody).then(response => {
				//const {model} = response.data;
				const {category} = response.data;
				let requestBody = (this.state.automatic) ? {
					model_id: this.stata.selectedModel,
					category_id: category.id,
					team_id: this.state.selectedTeam,
					status_id: this.state.selectedStatus,
					quantity: 1,
					auto_assign: "yes",
					company_id: this.state.company_id
				}: {
					model_id: this.state.selectedModel,
					category_id: category.id,
					team_id: this.state.selectedTeam,
					status_id: this.state.selectedStatus,
					quantity: this.state.quantity,
					auto_assign: "no",
					serials: this.state.equipmentserials,
					company_id: this.state.company_id	
				}
				console.log(JSON.stringify(requestBody, null, 3));
				return apiequipment.store(requestBody)
			}).then(response => {
				this.props.navigation.goBack();				
			}).catch(err => {
				console.log(JSON.stringify(err, null, 3));
			})			
		} else if (!newcategoryEnabled && newmodelEnabled) {
			const modelBody = {
				name: this.state.newmodel,
				category_id: this.state.selectedCategory,
				company_id: this.state.company_id
			};
			console.log("Model body", JSON.stringify(modelBody, null, 3));
			apimodels.store(modelBody)
			 	.then(response => {
				const {model} = response.data;
				let requestBody = (this.state.automatic) ? {
					model_id: model.id,
					category_id: this.state.selectedCategory,
					team_id: this.state.selectedTeam,
					status_id: this.state.selectedStatus,
					quantity: 1,
					auto_assign: "yes",
					company_id: this.state.company_id
				}: {
					model_id: model.id,
					category_id: this.state.selectedCategory,
					team_id: this.state.selectedTeam,
					status_id: this.state.selectedStatus,
					quantity: this.state.quantity,
					auto_assign: "no",
					serials: this.state.equipmentserials,
					company_id: this.state.company_id	
				}
				console.log("Request body", JSON.stringify(requestBody, null, 3));
				return apiequipment.store(requestBody)
			}).then(response => {
				this.props.navigation.goBack();				
			}).catch(err => {
				console.log(JSON.stringify(err, null, 3));
			})			
		} else {
				let requestBody = (this.state.automatic) ? {
					model_id: this.state.selectedModel,
					category_id: this.state.selectedCategory,
					team_id: this.state.selectedTeam,
					status_id: this.state.selectedStatus,
					quantity: 1,
					auto_assign: "yes",
					company_id: this.state.company_id
				}: {
					model_id: this.state.selectedModel,
					category_id: this.state.selectedCategory,
					team_id: this.state.selectedTeam,
					status_id: this.state.selectedStatus,
					quantity: this.state.quantity,
					auto_assign: "no",
					serials: this.state.equipmentserials,
					company_id: this.state.company_id	
				};
				console.log("Request body ", JSON.stringify(requestBody, null, 3));
			apiequipment.store(requestBody).then(response => {
				this.props.navigation.goBack();				
			}).catch(err => {
				console.log(JSON.stringify(err, null, 3));
			});
		}
		/*let requestBody = (this.state.automatic) ? {
			model_id: this.state.selectedModel,
			category_id: this.state.selectedCategory,
			team_id: this.state.selectedTeam,
			status_id: this.state.selectedStatus,
			quantity: 1,
			auto_assign: "yes",
			company_id: this.state.company_id
		}
			:
			{
				model_id: this.state.selectedModel,
				category_id: this.state.selectedCategory,
				team_id: this.state.selectedTeam,
				status_id: this.state.selectedStatus,
				quantity: this.state.quantity,
				auto_assign: "no",
				serials: this.state.equipmentserials,
				company_id: this.state.company_id
			}

		console.log("Request body ", requestBody);
		apiequipment.store(requestBody)
			.then(response => {
				console.log(response, null, 3);
				this.props.navigation.goBack();
			})
			.catch(err => {
				console.log(JSON.stringify(err, null, 3));
			})*/
			
	}

	refreshData() {
		Promise.all([
			apicategory.index(),
			apiteams.index(),
			apiStatus.index()
		])
			.then(responses => {
				const categories = responses[0].data.data;
				const teams = responses[1].data.data;
				const statuses = responses[2].data.data;
				console.log("Categories", categories);
				console.log("Teams", teams);
				console.log("statuses", statuses);
				const newcategories = [];
				const newteams = [];
				const newstatuses = [];
				categories.forEach(category => {
					const newcategory = {
						id: category.id,
						name: category.name
					}
					newcategories.push(newcategory);
				});
				console.log("New Categories", newcategories);

				teams.forEach(team => {
					const newteam = {
						id: team.id,
						name: team.name
					}
					newteams.push(newteam);
				});

				console.log("New teams", newteams);

				statuses.forEach(status => {
					const newstatus = {
						id: status.id,
						name: status.name
					}
					newstatuses.push(newstatus);
				});

				console.log("New statuses", newstatuses);

				const companyId = categories[0].company_id;

				this.setState({
					categories: [...this.state.categories, ...newcategories],
					teams: [...this.state.teams, ...newteams],
					statuses: [...this.state.statuses, ...newstatuses],
					company_id: companyId
				})
			})
			.catch(err => {
				console.log(JSON.stringify(err, null, 3));
			});
	}

	refreshModel() {
		apimodels.categorizedModels(this.state.selectedCategory)
			.then(response => {
				const models = response.data.data;
				console.log("Models", JSON.stringify(models, null, 3));
				const newarray = [];
				models.forEach(model => {
					const newmodel = {
						id: model.id,
						name: model.name,
					}
					newarray.push(newmodel);
				});
				this.setState({
					models: [...this.state.models, ...newarray],
					newcategoryEnabled: false,
					newprefixEnabled: false
				})
			})
			.catch(err => {
				console.log(JSON.stringify(err, null, 3));
			});
	}

	showModal() {
		this.setState({
			visibleModal: true
		});
	}

	dismissModal() {
		this.setState({
			visibleModal: false,
			automatic: true
		});
	}

	componentWillMount() {
		this.subs = [
			this.props.navigation.addListener("didFocus", () => {
				this.props.changeHeaderTitle("Add new Equipment");
				this.props.visableSideBar();
				this.refreshData();
				console.log("Insert equipments mounted");
			}),
		];
	}

	componentDidMount() {
	}

	componentWillUnmount() {
		this.subs.forEach(sub => sub.remove());
	}

	render() {
		return (
			<Container>
				<Content padder>
					<Form>
						<Item picker>
							<Label>
								Select category
							</Label>
							<Picker
								mode="dropdown"
								iosIcon={<Icon name="ios-arrow-down-outline" />}
								style={{
									width: undefined
								}}
								placeHolder="Select category"
								placeholderStyle={{ color: "#bfc6ea" }}
								placeholderIconColor="#007aff"
								selectedValue={this.state.selectedCategory}
								onValueChange={(value) => {
									if (value !== 0) {
										console.log(value);
										console.log(typeof (value));
										this.setState({
											selectedCategory: value,
										}, () => {
											this.refreshModel();
										});
									} else {
										this.setState({
											selectedCategory: value,
											newcategoryEnabled: true,
											newprefixEnabled: true
										});
									}
								}}
							>
								{
									(this.state.categories.length > 0)
										? this.state.categories.map((category, index) => {
											return <Picker.Item label={category.name} value={category.id} key={index} />
										})
										: null
								}
							</Picker>
						</Item>

						<Item floatingLabel>
							<Label>
								Category
							</Label>
							<Input disabled={!this.state.newcategoryEnabled} value={this.state.newcategory} onChangeText={value => {
								this.setState({
									newcategory: value
								});
							}} />
						</Item>

						<Item floatingLabel>
							<Label>
								Prefix
							</Label>
							<Input disabled={!this.state.newprefixEnabled} value={this.state.newprefix} onChangeText={value => {
								this.setState({
									newprefix: value
								});
							}} />
						</Item>

						<Item picker>
							<Label>
								Make / Model
							</Label>
							<Picker
								mode="dropdown"
								iosIcon={<Icon name="ios-arrow-down-outline" />}
								style={{
									width: undefined
								}}
								placeHolder="Select Make/Model"
								placeholderStyle={{ color: "#bfc6ea" }}
								placeholderIconColor="#007aff"
								selectedValue={this.state.selectedModel}
								onValueChange={(value) => {
									if (value !== 0) {
										this.setState({
											selectedModel: Number(value),
											newmodelEnabled: false
										})
									} else {
										this.setState({
											selectedModel: Number(value),
											newmodelEnabled: true
										});
									}
								}}
							>
								{
									(this.state.models.length > 0)
										? this.state.models.map((model, index) => {
											return <Picker.Item label={model.name} value={model.id} key={index} />
										})
										: null
								}
							</Picker>
						</Item>

						<Item floatingLabel>
							<Label>
								Make/Model
							</Label>
							<Input disabled={!this.state.newmodelEnabled} value={this.state.newmodel} onChangeText={value => {
								this.setState({
									newmodel: value
								});
							}} />
						</Item>

						<Item picker>
							<Label>
								Select team
							</Label>
							<Picker
								mode="dropdown"
								iosIcon={<Icon name="ios-arrow-down-outline" />}
								style={{
									width: undefined
								}}
								placeHolder="Select team"
								placeholderStyle={{ color: "#bfc6ea" }}
								placeholderIconColor="#007aff"
								selectedValue={this.state.selectedTeam}
								onValueChange={(value) => {
									this.setState({
										selectedTeam: Number(value),
									});
								}}
							>
								{
									(this.state.teams.length > 0)
										? this.state.teams.map((team, index) => {
											return <Picker.Item label={team.name} value={team.id} key={index} />
										})
										: null
								}
							</Picker>
						</Item>

						<Item picker>
							<Label>
								Select statuses
							</Label>
							<Picker
								mode="dropdown"
								iosIcon={<Icon name="ios-arrow-down-outline" />}
								style={{
									width: undefined
								}}
								placeHolder="Select status"
								placeholderStyle={{ color: "#bfc6ea" }}
								placeholderIconColor="#007aff"
								selectedValue={this.state.selectedStatus}
								onValueChange={(value) => {
									this.setState({
										selectedStatus: Number(value),
									});
								}}
							>
								{
									(this.state.statuses.length > 0)
										? this.state.statuses.map(status => {
											return <Picker.Item label={status.name} value={status.id} key={status.id} />
										})
										: null
								}
							</Picker>
						</Item>

						{/* <Item floatingLabel>
							<Label>
								Quantity
							</Label>
							<Input value={this.state.quantity} keyboardType="numeric" onChangeText={value => {
								this.setState({
									quantity: value
								});
							}} />
						</Item> */}



						<View style={{
							justifyContent: 'space-evenly',
							alignContent: 'center'
						}}>
							<Item style={{
								margin: 5
							}}>
								<View style={{
									flex: 1,
									flexDirection: "column",
									justifyContent: "space-between",
									alignContent: "center",
									alignItems: 'center'
								}}>
									{/* <CheckBox checked={this.state.automatic} onPress={(checked) => {
										this.setState({
											automatic: !this.state.automatic,
										}, () => this.showModal());
									}} /> */}

									<Switch
										value={this.state.automatic}
										onValueChange={(value) => {
											if (!value) {
												this.setState({
													automatic: false
												}, () => this.showModal())
											} else {
												this.setState({
													automatic: true
												});
											}
										}}
									/>

									<Text style={{
										fontSize: 11,
									}}>Assign Equipment Numbers Automatically?</Text>
								</View>
							</Item>
						</View>

						{/* <Item>
							{
								(!this.state.automatic && this.state.quantity)
									? <List>
										{
											[...Array(Number(this.state.quantity))].map((_, i) => {
												<ListItem>
													<Item success>
														<Input placeholder='' />
														<Icon name='checkmark-circle' />
													</Item>
												</ListItem>
											})
										}
									</List> : null
							}
						</Item> */}


					</Form>
					<View style={{
						flex: 1,
						justifyContent: 'space-evenly',
						alignContent: 'center'
					}}>
						<Button full success style={{
							margin: 5
						}} onPress={() => {
							this.postequipmentBody();
						}}>
							<Text>submit</Text>
						</Button>

						<Button full waring style={{
							margin: 5
						}} onPress={() => {
							this.props.navigation.goBack();
						}}>
							<Text>Cancel</Text>
						</Button>
					</View>
				</Content>

				<Modal
					visible={this.state.visibleModal}
					transparent={false}
					animationType={"slide"}
					onRequestClose={this.dismissModal}
				>

					<Content>
						<View>
							<Title>
								Equipment serail numbers
						</Title>
						</View>
						<Item pikcer>
							<Label>
								Select number of rows to add
							</Label>

							<Picker
								mode="dropdown"
								iosIcon={<Icon name="ios-arrow-down-outline" />}
								style={{
									width: undefined
								}}
								selectedValue={this.state.quantity}
								onValueChange={(value) => {
									if (value !== 0) {
										console.log(value);
										console.log(typeof (value));
										this.setState({
											quantity: value,
										});
									}
								}}>
								<Picker.Item label="1" value={1} key="1" />
								<Picker.Item label="2" value={2} key="2" />
								<Picker.Item label="3" value={3} key="3" />
								<Picker.Item label="4" value={4} key="4" />
								<Picker.Item label="5" value={5} key="5" />
								<Picker.Item label="6" value={6} key="6" />
								<Picker.Item label="7" value={7} key="7" />
								<Picker.Item label="8" value={8} key="8" />
								<Picker.Item label="9" value={9} key="9" />
								<Picker.Item label="10" value={10} key="10" />
							</Picker>
						</Item>
						<Content>
							<Form>
								{
									(this.state.quantity && Number(this.state.quantity) > 0)

										?

										[...Array(Number(this.state.quantity))].map((_, i) => {
											return (<Item floatingLabel>
												<Label>
													{"Equipment no." + String(Number(i) + 1)}
												</Label>
												<Input keyboardType="numeric" onChangeText={(text) => {
													const data = {
														id: Number(i) + 1,
														value: text,
														validate: "true",
														reason: "Please Input Serial Number"
													};
													//var variableList = this.state.equipmentserails.filter(x => data.id !== x.id);
													//console.log(JSON.stringify(variableList, null, 3));
													this.setState({
														equipmentserials: [...this.state.equipmentserials.filter(x => data.id !== x.id), data]
													});
												}} />
											</Item>)
										})
										: <Text>No quantity entered</Text>
								}

							</Form>

							<Button full warning onPress={() => {
								console.log(JSON.stringify(this.state.equipmentserials, null, 3));
								this.postequipmentBody();
							}} style={{
								margin: 5
							}}>
								<Text>Save</Text>
							</Button>

							<Button full error onPress={() => {
								this.dismissModal();
							}} style={{
								margin: 5
							}}>
								<Text>Cancel</Text>
							</Button>
						</Content>
					</Content>

					{/* <List>
						{
							(this.state.quantity && Number(this.state.quantity) > 0)
								? Array(Number(this.state.quantity)).map((_, index) => {
									<ListItem>
										return <Item success>
											<Input value="" onChangeText={(text) => {
												console.log("Value for indexed", text);
											}} />
										</Item>
									</ListItem>
								})
								: null
						}
					</List> */}
				</Modal>
			</Container>
		);
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		changeHeaderTitle: (headerTitle) => {
			dispatch(changeHeaderTitle(headerTitle))
		},
		visableSideBar: () => {
			dispatch(changeSideBarInivisble(false))
		},
	}
}

export default connect(undefined, mapDispatchToProps)(InsertEquipment);
