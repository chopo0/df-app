import React from 'react';
import {
    connect
} from 'react-redux';
import {
    Content,
    Button,
    Item,
    Input,
    Form,
    Text,
    Toast,
    CheckBox
} from "native-base";
import {
    View
} from 'react-native';
import { CreditCardInput, LiteCreditCardInput } from "react-native-credit-card-input";
import stripe from 'tipsi-stripe';


import apiAccount from '../../../../api/account'
import {
    STRIPE_PUB_KEY, STRIPE_SEC_KEY
} from '../../../../config/env'
import BaseComponent from '../../BaseComponent'
import {
    changeHeaderTitle,
    changeSideBarInivisble
} from '../../../../actions/shared'
import {
    fetchUser
} from '../../../../service/fetch_user'
import {
    handleErrorResponse
} from '../../../../service/error_handler'
import NavigationService from '../../../../service/NavigationService';
import styles from "./styles"

stripe.setOptions({
    publishableKey: STRIPE_PUB_KEY,
});

class CreditCard extends BaseComponent {
    constructor(props) {
        super(props)
        this.state = {
            form: {
                valid: false,
                values: {},
                status: {}
            }
        }
        
        this._onChange = this._onChange.bind(this);
    }

    componentDidMount() {
        this.subs = [
            this.props.navigation.addListener("didFocus", () => {  
                this.props.invisableSideBar();
                console.log("credit card page mounted")
                this.props.changeHeaderTitle("Update Credit Card Info");
                this.loaded();
            }),
            this.props.navigation.addListener("willBlur", () => {})
        ];
    }

    componentWillUnmount() {
        this.subs.forEach(sub => sub.remove());
    }

    _onChange(form) {
        this.setState((preState) => ({
            form: Object.assign({}, preState.form, form)
        }));
    }

    subscribe() {
        if (!this.state.form.valid || this.state.isRunning) return;

        let self = this;
        let cardInfo = this.state.form.values;
        let exp = cardInfo.expiry.split("/");
        let number = cardInfo.number.replace(/\s/g, '');
        this.run();
        stripe.createTokenWithCard({
            number: number,
            expMonth: parseInt(exp[0]),
            expYear: parseInt(exp[1]),
            cvc: cardInfo.cvc
        }).then((res) => {
            res.id = res.tokenId;
            res.object = "token";
            res.type = "card";
            res.card.id = res.card.cardId;
            apiAccount.updateSource({'stripeToken' : res})
                .then(response => {
                    Toast.show({
                        text: response.data.message,
                        duration: 3000,
                        type: "success"
                    })
                    self.dataReady();
                    NavigationService.navigate('Company', {})
                })
                .catch(err => {
                    self.dataFailed();
                    handleErrorResponse(error)
                })
            console.log(res)
        }).catch(e => {
            this.dataFailed();
            handleErrorResponse({
                data: {
                    message: 'Whoops! Something wrong'
                }
            })
        });
    }
    
    render() {
        return (
            <Content padder style={{ paddingTop: 30 }}>
                <CreditCardInput style={{ marginTop: 30 }} onChange={this._onChange} />
                <Button disabled={!this.state.form.valid || this.state.isRunning} info={this.state.form.valid && !this.state.isRunning} block style={{ margin: 15, marginTop: 30 }} onPress={()=> this.subscribe()} >
                    <Text>Update</Text>
                </Button>
            </Content>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user.user,
        company: state.user.company
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        changeHeaderTitle: (headerTitle) => {
            dispatch(changeHeaderTitle(headerTitle))
        },
        invisableSideBar: () => {
            dispatch(changeSideBarInivisble(true))
        },
        fetchUser: () => {
            dispatch(fetchUser())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreditCard);