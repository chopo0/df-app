import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    Container,
    Content,
    List,
    ListItem,
    Item,
    Button,
    Text,
    Toast,
    CheckBox,
    Body,
    Form,
    Input,
    Fab,
    Label,
    Icon,
    Row,
    Col
} from "native-base";
import {
    View,
    ScrollView,
    Alert,
    PanResponder,
    Animated
} from 'react-native'
import _ from 'lodash'
import EventEmitter from 'EventEmitter';
import SortableList from 'react-native-sortable-list';

import {
    changeHeaderTitle,
    changeSideBarInivisble
} from '../../../../actions/shared'
import {
    handleErrorResponse
} from '../../../../service/error_handler'
import BaseComponent from '../../BaseComponent'
import LoadingComponent from '../../BaseComponent/loading'
import LegalDisclaimer from '../legal_disclaimer'
import styles from "./styles"
import NavigationService from '../../../../service/NavigationService';
import ScopeListItem from './list-item'
import DraggableArea from "react-native-dnd-grid"

import { fetchFormsOrder } from '../../../../service/standard_form'
import apiStandardForm from '../../../../api/standard_form'
import apiStandardScope from '../../../../api/standard_scope'
import apiUom from '../../../../api/uom'

class ScopeList extends BaseComponent {
    constructor(props) {
        super(props)

        this.state = {
            dragging: false,
            pending: false,
            pageIndex: '0',
            scopes: [],
            currentOrder: []
        }
    }

    componentWillMount() {
        this.setState({
            pageIndex: this.props.pageIndex,
            scopes: this.props.scopes,
            currentOrder: []
        })
    }

    componentDidMount() {
    }

    componentWillReceiveProps(props) {
        if (props.pageIndex !== this.state.pageIndex) {
            this.setState({
                pageIndex: props.pageIndex,
                scopes: props.scopes,
                currentOrder: []
            })
        }
    }

    componentWillUnmount() {
    }

    renderRow = ({ data, active }) => {
        return <ScopeListItem
            style={{ width: '100%' }}
            uoms={this.props.uoms}
            item={data}
            active={active}
            updateScopeData={this.updateScopeData}
        />
    }

    changeOrder = _.debounce((nextOrder) => {
        let scopes = []
        _.forEach(nextOrder, scopeIndex => {
            scopes.push(
                this.state.scopes[scopeIndex]
            )
        })
        this.setState({
            currentOrder: nextOrder
        })
        this.saveScopes(scopes)
    }, 500)

    updateScopeData = _.debounce((scope) => {
        let scopes = []
        if (this.state.currentOrder.length === 0) {
            scopes = this.state.scopes
        } else {
            _.forEach(this.state.currentOrder, scopeIndex => {
                scopes.push(
                    this.state.scopes[scopeIndex]
                )
            })
        }
        let index = _.findIndex(scopes, (item) => { return scope.id == item.id })
        scopes[index] = scope
        this.saveScopes(scopes)
    }, 500)

    saveScopes = (scopes) => {
        const apis = [
            apiStandardScope.store({
                scopes: scopes
            })
        ]
        Promise.all(apis)
            .then(response => {
            }).catch(handleErrorResponse)
    }

    deletePage() {
        this.props.deletePage({
            pageIndex: this.props.pageIndex
        })
    }

    render() {
        return (
            <React.Fragment>
                {
                    this.state.pageIndex !== 'misc' ?
                        (
                            <View style={{ width: '100%', paddingHorizontal: 30, justifyContent: 'flex-end', flexDirection: 'row' }}>
                                <Button style={{ borderBottomColor: 'rgba(0,0,0,0.1)', borderBottomWidth: 1 }} iconRight transparent danger onPress={() => this.deletePage()}>
                                    <Text style={{ color: 'rgb(0,0,0)', fontSize: 16 }}>Page: {this.state.pageIndex}</Text>
                                    <Icon style={{}} active name="md-close" />
                                </Button>
                            </View>
                        )
                        : (
                            <View style={{ width: '100%', paddingHorizontal: 30, justifyContent: 'flex-end', flexDirection: 'row' }}>
                                <Button style={{ borderBottomColor: 'rgba(0,0,0,0.1)', borderBottomWidth: 1 }} iconRight transparent danger>
                                    <Text style={{ color: 'rgb(0,0,0)', fontSize: 16 }}> Misc Page </Text>
                                </Button>
                            </View>
                        )
                }
                {this.state.scopes && this.state.scopes.length > 0 ?
                    <SortableList
                        style={styles.list}
                        contentContainerStyle={styles.contentContainer}
                        data={this.state.scopes}
                        renderRow={this.renderRow}
                        onChangeOrder={(nextOrder) => this.changeOrder(nextOrder)} />
                    : null
                }
            </React.Fragment>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchFormsOrder: () => {
            dispatch(fetchFormsOrder())
        }
    }
}

export default connect(undefined, mapDispatchToProps)(ScopeList);