import {
    INIT_PAGINATION,
    GO_PAGE
} from '../actions/actionTypes'

const initialState = {
    pageNum: "1",
    pageCount: 0
}

export default function pagination(state = initialState, action) {
    switch (action.type) {
        case INIT_PAGINATION:
            if (action.isPageNumInit) {
                return Object.assign({}, state, {
                    pageCount: action.pageCount,
                    pageNum: (action.pageCount > 0 ? "1" : "0")
                });
            } else {
                return Object.assign({}, state, {
                    pageCount: action.pageCount
                });
            }
        case GO_PAGE:
            pageNum = action.pageNum || 1;
            pageNum = (pageNum > action.pageCount) ? action.pageCount : pageNum
            pageNum = "" + pageNum
            return Object.assign({}, state, {
                pageNum: pageNum
            });
        default:
            return state;
    }
}