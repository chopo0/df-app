var React = require("react-native");
var {
    Dimensions
} = React;

import commonColor from "../../../../../../theme/variables/commonColor";

export default {
    listView: {
        borderBottomColor: "#D9D5DC",
        flexDirection: 'row',
        borderBottomWidth: 1
    },
    defaultText: {
        paddingHorizontal: 10,
        paddingVertical: 5
    },
    defaultCheckBoxWrapper: {
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center'
    }
};