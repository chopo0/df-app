import {
    SET_PROJECT,
    SET_PROJECT_FORMS,
    SET_PROJECT_SIDEBAR
} from './actionTypes'

export const setProject = (projectId, callReport = null) => {
    return {
        type: SET_PROJECT,
        projectId: projectId,
        callReport: callReport
    }
}

export const setProjectForms = (projectId, projectForms) => {
    return {
        type: SET_PROJECT_FORMS,
        projectId: projectId,
        projectForms: projectForms
    }
}

export const setProjectSidebar = (projectId, projectSidebar) => {
    return {
        type: SET_PROJECT_SIDEBAR,
        projectId: projectId,
        projectSidebar: projectSidebar
    }
}