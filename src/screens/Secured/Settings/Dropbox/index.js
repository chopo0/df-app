import React, { Component } from 'react';
import {
    connect
} from 'react-redux';
import {
    Content,
    Button,
    Item,
    Input,
    Form,
    Text,
    Toast,
    CheckBox
} from "native-base";
import { WebView } from 'react-native';

import BaseComponent from '../../BaseComponent'
import {
    changeHeaderTitle,
    changeSideBarInivisble
} from '../../../../actions/shared'
import {
    fetchUser
} from '../../../../service/fetch_user'
import {
    API_DOMAIN,
    DROPBOX_CLIENT_ID
} from '../../../../config/env'
import {
    handleErrorResponse
} from '../../../../service/error_handler'
import NavigationService from '../../../../service/NavigationService';
import apiCompanies from '../../../../api/company'
import _ from 'lodash'

class Dropbox extends BaseComponent {
    constructor(props) {
        super(props)
        this.state = {
            authUrl: ""
        }
    }

    componentDidMount() {
        this.subs = [
            this.props.navigation.addListener("didFocus", () => {                
                this.setState({
                    authUrl: 'https://www.dropbox.com/oauth2/authorize?response_type=token&client_id=' + DROPBOX_CLIENT_ID + '&redirect_uri=' + API_DOMAIN + '/dropbox-auth'
                })
                this.props.invisableSideBar();
                console.log("dropbox integration page mounted")
                this.props.changeHeaderTitle("Dropbox Authorizing...");
                this.loaded();
            }),
            this.props.navigation.addListener("willBlur", () => {})
        ];
    }

    componentWillUnmount() {
        this.subs.forEach(sub => sub.remove());
    }

    _onMessage = (event) => {
        if (_.includes(event.nativeEvent.data, "You have successfully granted access to your Dropbox Account.")) {
            NavigationService.navigate('Company', {})
        }
    }

    render() {
        const jsCode = `
            window.waitForBridge = function(fn) { 
                return (window.postMessage.length === 1) ? fn() : 
                    setTimeout(function() { 
                        window.waitForBridge(fn) 
                    }, 5) }; 
            window.waitForBridge(function() {
                window.postMessage(document.body.innerHTML)
            });
        `
        return (
                <WebView
                    javaScriptEnabled={true}
                    source={{uri: this.state.authUrl}}
                    startInLoadingState={true}
                    onMessage={(event) => this._onMessage(event)}
                    injectedJavaScript={jsCode}
                />
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user.user,
        company: state.user.company
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        changeHeaderTitle: (headerTitle) => {
            dispatch(changeHeaderTitle(headerTitle))
        },
        invisableSideBar: () => {
            dispatch(changeSideBarInivisble(true))
        },
        fetchUser: () => {
            dispatch(fetchUser())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dropbox);