import {
    Platform,
    Dimensions
} from 'react-native';

const window = Dimensions.get('window');

import commonColor from "../../../../theme/variables/commonColor";

export default {
    container: {
        flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
        backgroundColor: '#eee',

        ...Platform.select({
            ios: {
                paddingTop: 20,
            },
        }),
    },

    title: {
        fontSize: 20,
        paddingVertical: 20,
        color: '#999999',
    },

    list: {
        flex: 1,
        width: '100%'
    },

    contentContainer: {
        width: '100%',

        ...Platform.select({
            ios: {
                paddingHorizontal: 30,
            },

            android: {
                paddingHorizontal: 0,
            }
        })
    },

    row: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#fff',
        padding: 0,
        flex: 1,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderColor: '#D9D5DC',

        ...Platform.select({
            ios: {
                width: '94%',
                shadowColor: 'rgba(0,0,0,0.2)',
                shadowOpacity: 1,
                shadowOffset: {
                    height: 2,
                    width: 2
                },
                shadowRadius: 2,
            },

            android: {
                width: '94%',
                elevation: 0,
                marginHorizontal: '3%',
            },
        })
    },

    defaultText: {
        textAlign: 'center',
        paddingHorizontal: 10,
        paddingVertical: 5,
        fontSize: 16,
        paddingVertical: 3,
        height: 28
    },

    defaultInput: {
        textAlign: 'center',
        fontSize: 16,
        padding: 0,
        height: 28
    },

    defaultPicker: {
        height: 28,
        padding: 0,
        transform: [{
                scaleX: 1
            },
            {
                scaleY: 1
            },
        ]
    },

    defaultBtn: {
        height: 28,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    leftBorderCol: {
        borderLeftWidth: 1,
        borderColor: "#D9D5DC"
    },

    rightBorderCol: {
        borderRightWidth: 1,
        borderColor: "#D9D5DC"
    },

    colPadAdj: {
        paddingVertical: 5
    },

    headerFont: {
        fontWeight: 'bold'
    },

    text: {
        textAlign: 'center',
        fontSize: 16,
        color: '#222222',
    },
    col1: {
        // backgroundColor: 'blue'
    },
    col2: {
        // backgroundColor:'red',
        justifyContent: 'center',
        height: 25,
    },
    col3: {
        height: 25,
    },
    colItem1: {
        borderWidth: 0,
    },
    colItem2: {
        borderWidth: 0,
    },
    itemDivider: {
        backgroundColor: "#a8d9d5",
        paddingVertical: 0,
        height: 30
    },
    picker: {
        height: 25,
        transform: [{
                scaleX: 0.9
            },
            {
                scaleY: 0.9
            },
        ]
    },
    picker1: {
        height: 32,
        backgroundColor: '#a8d9d5'
    },
    picker2: {
        height: 32,
        backgroundColor: '#a8d9d5',
        // color: 'white'
    },
    location: {
        height: 32,
        // borderColor: '#b85dd2',
        // color: 'white'
    },
    validate: {
        height: 32,
        justifyContent: 'center',
        alignItems: 'center',
        width: 44
    },
    navBtn: {
        margin: 0,
        flex: 1,
        justifyContent: 'center'
    },
    dlgBg: {
        backgroundColor: commonColor.androidRippleColor
    }
}