var React = require("react-native");
var {
    Dimensions
} = React;

import commonColor from "../../../../theme/variables/commonColor";

export default {
    listView: {
        borderBottomColor: "#D9D5DC",
        flexDirection: 'row',
        borderBottomWidth: 1
    },
    defaultText: {
        textAlign: 'center',
        borderRightWidth: 1,
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderRightColor: "#D9D5DC",
        backgroundColor: "#f6f6f6",
    },
    text: {
        textAlign: 'center',
        paddingHorizontal: 10,
        paddingVertical: 5
    },
    defaultHeaderText: {
        textAlign: 'center',
        borderRightWidth: 1,
        paddingVertical: 5,
        borderRightColor: "#D9D5DC",
        backgroundColor: "#5898F6",
        fontWeight: 'bold',
        color: 'white'
    }
};