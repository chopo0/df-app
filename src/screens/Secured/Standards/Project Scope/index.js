import React from 'react';
import { connect } from 'react-redux';
import {
    ListItem,
    Item,
    Button,
    Text,
    CheckBox,
    Body,
    Form,
    Input,
    Fab,
    Label,
    Icon
} from "native-base";
import {
    View,
    Alert,
    Animated,
    Easing
} from 'react-native'
import _ from 'lodash'
import EventEmitter from 'EventEmitter';
import GestureRecognizer from 'react-native-swipe-gestures';

import {
    changeHeaderTitle,
    changeSideBarInivisble
} from '../../../../actions/shared'
import {
    handleErrorResponse
} from '../../../../service/error_handler'
import BaseComponent from '../../BaseComponent'
import LoadingComponent from '../../BaseComponent/loading'
import LegalDisclaimer from '../legal_disclaimer'
import styles from "./styles"
import ScopeList from './list'

import { fetchFormsOrder } from '../../../../service/standard_form'
import apiStandardForm from '../../../../api/standard_form'
import apiStandardScope from '../../../../api/standard_scope'
import apiUom from '../../../../api/uom'

class ProjectScope extends BaseComponent {
    constructor(props) {
        super(props)

        this.state = {
            scopes: {},
            miscScopes: [],
            form: {},
            uoms: [],
            activeFab: false,
            currentPage: "1",
            isMisc: false,
            count: 0
        }

        this.init = this.init.bind(this)
        this.addNewPage = this.addNewPage.bind(this)
        this.updateForm = _.debounce(this.updateForm.bind(this), 2000)
        this.goPageImediately = this.goPageImediately.bind(this)

        this.animatedValue = new Animated.Value(0)
        this._emitter = new EventEmitter()
    }

    componentDidMount() {
        this.subs = [
            this.props.navigation.addListener("didFocus", () => {
                this.props.changeHeaderTitle("Project Scope");
                this.props.visableSideBar();
                this.init()
                this._emitter.emit('parent component mounted');
                console.log("Project Scope mounted")
            }),
            this.props.navigation.addListener("willBlur", () => { })
        ]
    }

    componentWillUpdate() {
    }

    componentDidUpdate() {
    }

    componentWillUnmount() {
        this.subs.forEach(sub => sub.remove());
        this._emitter.removeAllListeners();
    }

    init() {
        this.run()
        let formId = 2
        const apis = [
            apiStandardScope.index(),
            apiStandardForm.show(formId),
            apiUom.index()
        ]

        this.setState({
            isLoaded: false
        }, () => {
            return Promise.all(apis)
                .then(response => {
                    this.setState({
                        scopes: response[0].data.scopes || [],
                        miscScopes: response[0].data.misc_scopes,
                        form: response[1].data.form,
                        uoms: response[2].data.uoms,
                        count: Object.keys(response[0].data.scopes).length,
                        currentPage: "1",
                        isMisc: false
                    })
                    this.loaded()
                    this.dataReady()
                }).catch(err => {
                    this.dataFailed()
                    handleErrorResponse(err)
                })
        })
    }

    goPageImediately(pageNum) {
        pageNum = pageNum || 1;
        pageNum = (pageNum > this.state.count) ? this.state.count : pageNum
        pageNum = "" + pageNum
        this.setState({
            currentPage: pageNum,
            isMisc: false
        })
        // this.slide()
    }

    setInputState = (property, value, isImediate = false) => {
        this.setState((preState) => ({
            form: Object.assign({}, preState.form, {
                [property]: value
            })
        }), () => {
            if (isImediate) {
                this.updateFormImediately();
            } else {
                this.updateForm();
            }
        });
    }

    updateForm = () => {
        this.updateFormImediately()
    }

    updateFormImediately = () => {
        this.run()
        let form = this.state.form
        apiStandardForm.patch(form.id, form)
            .then(() => {
                this.props.fetchFormsOrder()
                this.dataReady();
            }).catch(error => {
                handleErrorResponse(error)
                this.dataFailed();
            })
    }

    addNewPage = () => {
        this.run()
        apiStandardScope.addPage()
            .then(() => {
                this.init()
            }).catch(error => {
                this.dataFailed()
                handleErrorResponse(error)
            })
    }

    revertScope = () => {
        Alert.alert(
            'Revert Standard Scope',
            'Are you sure you want to revert to the default scopes?',
            [
                { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                {
                    text: 'Confirm', onPress: () => {
                        this.run()
                        apiStandardScope.revert()
                            .then(() => {
                                this.init(true)
                            })
                            .catch(handleErrorResponse)
                    }
                },
            ],
            { cancelable: false }
        )
    }

    changed = () => {
    }

    deletePage = (payload) => {
        this.run()
        apiStandardScope.removePage({ page: payload.pageIndex })
            .then(() => {
                this.init()
            }).catch(handleErrorResponse)
    }

    onSwipeLeft() {
        if (Number(this.state.currentPage) >= this.state.count) {
            this.setState({
                isMisc: true,
                currentPage: "" + this.state.count
            })
            // this.slide()
            return
        }
        this.goPageImediately(Number(this.state.currentPage) + 1)
    }

    onSwipeRight() {
        if (this.state.isMisc) {
            this.goPageImediately(Number(this.state.currentPage))
        } else {
            this.goPageImediately(Number(this.state.currentPage) - 1)
        }
    }

    slide = () => {
        this.animatedValue.setValue(-100)
        Animated.spring(this.animatedValue, {
            toValue: 0,
            duration: 3000,
            easing: Easing.linear
        }).start();
    };

    render() {
        let bgColor = this.state.isMisc ? '#fa8072' : '#28B8F6'
        const config = {
            velocityThreshold: 0.1,
            directionalOffsetThreshold: 30
        };

        return (
            this.isLoaded() && !this.state.isRunning ? (
                <View style={styles.container}>
                    <LegalDisclaimer emitter={this._emitter}></LegalDisclaimer>
                    <Form>
                        <Item>
                            <Label>*Enter side menu name: </Label>
                            <Input onChangeText={(text) => this.setInputState("name", text)} value={this.state.form.name} />
                        </Item>
                        <Item>
                            <Label>*Enter form title: </Label>
                            <Input onChangeText={(text) => this.setInputState("title", text)} value={this.state.form.title} />
                        </Item>
                    </Form>
                    <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 10, flexDirection: 'row' }}>
                        <Item style={{ width: 300 }}>
                            <Button small block danger={this.state.isMisc} info={!this.state.isMisc} style={styles.navBtn}
                                onPress={() => this.goPageImediately(1)}
                                disabled={Number(this.state.currentPage) <= 1}
                            >
                                <Text style={{ textAlign: 'center' }}> {'<<'} </Text>
                            </Button>
                            <Button small block danger={this.state.isMisc} info={!this.state.isMisc} style={styles.navBtn}
                                onPress={() => this.goPageImediately(Number(this.state.currentPage) - 1)}
                                disabled={Number(this.state.currentPage) <= 1}
                            >
                                <Text> {'<'} </Text>
                            </Button>
                            <Input keyboardType='numeric' style={{ fontSize: 15, paddingVertical: 0, flex: 1.5, textAlign: 'center', height: 32, backgroundColor: bgColor, color: 'white' }} value={this.state.currentPage} onChangeText={(text) => {
                                this.goPageImediately(Number(text))
                            }}></Input>
                            <Button small block danger={this.state.isMisc} info={!this.state.isMisc} style={styles.navBtn}
                                onPress={() => this.goPageImediately(Number(this.state.currentPage) + 1)}
                                disabled={Number(this.state.currentPage) >= this.state.count}
                            >
                                <Text> {'>'} </Text>
                            </Button>
                            <Button small block danger={this.state.isMisc} info={!this.state.isMisc} style={styles.navBtn}
                                onPress={() => this.goPageImediately(this.state.count)}
                                disabled={Number(this.state.currentPage) >= this.state.count}
                            >
                                <Text> {'>>'} </Text>
                            </Button>
                        </Item>
                        <Item style={{ width: 100, marginLeft: 10, marginTop: -3 }}>
                            <Button small block danger={!this.state.isMisc} info={this.state.isMisc} style={styles.navBtn}
                                onPress={() => {
                                    this.setState({
                                        isMisc: !this.state.isMisc
                                    })
                                    // this.slide()
                                }}
                            >
                                <Text> {'Misc'} </Text>
                            </Button>
                        </Item>
                    </View>
                    {/* <GestureRecognizer
                        onSwipeLeft={(state) => this.onSwipeLeft()}
                        onSwipeRight={(state) => this.onSwipeRight()}
                        config={config}
                        style={{
                            flex: 1
                        }}
                    > */}
                    {/* <Animated.View
                        style={[{
                            flex: 1,
                            transform: [
                                {
                                    translateX: this.animatedValue
                                }
                            ]
                        }]}
                    > */}
                    {this.state.isMisc == true ? (
                        <React.Fragment>
                            <ScopeList style={{ width: '100%' }}
                                scopes={this.state.miscScopes}
                                pageIndex="misc"
                                uoms={this.state.uoms}
                                form={this.state.form}
                                changed={this.changed}
                            ></ScopeList>
                            <ListItem>
                                <CheckBox checked={this.state.form.additional_notes_show == 1} onPress={() => this.setInputState("additional_notes_show", 1 - Number(this.state.form.additional_notes_show), true)} />
                                <Body>
                                    <Text>Additional notes.(Select if you wish to have Additional notes text box)</Text>
                                </Body>
                            </ListItem>
                        </React.Fragment>
                    ) : (
                            <ScopeList style={{ width: '100%' }}
                                scopes={this.state.scopes[this.state.currentPage]}
                                pageCount={this.state.count}
                                pageIndex={this.state.currentPage}
                                uoms={this.state.uoms}
                                form={this.state.form}
                                changed={this.changed}
                                deletePage={this.deletePage}
                            ></ScopeList>
                        )
                    }
                    {/* </Animated.View> */}
                    {/* </GestureRecognizer> */}
                    <Fab style={{ backgroundColor: '#5067FF' }}
                        active={this.state.activeFab}
                        direction="left"
                        position="bottomRight"
                        onPress={() => this.setState({ activeFab: !this.state.activeFab })}
                    >
                        <Icon name="settings" />
                        <Button style={{ backgroundColor: '#3B5998' }} onPress={() => this.addNewPage()} disabled={this.state.isRunning}>
                            <Icon name="md-add" />
                        </Button>
                        <Button style={{ backgroundColor: '#DD5144' }} onPress={() =>
                            this.revertScope()}>
                            <Icon name="ios-refresh" />
                        </Button>
                    </Fab>
                </View >
            ) : (
                    <LoadingComponent />
                )
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        changeHeaderTitle: (headerTitle) => {
            dispatch(changeHeaderTitle(headerTitle))
        },
        visableSideBar: () => {
            dispatch(changeSideBarInivisble(false))
        },
        fetchFormsOrder: () => {
            dispatch(fetchFormsOrder())
        }
    }
}

export default connect(undefined, mapDispatchToProps)(ProjectScope);