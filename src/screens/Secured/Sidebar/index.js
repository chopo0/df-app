import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Content, List, ListItem, Button, Text, Left, Right, Icon } from 'native-base';
import _ from 'lodash'

import commonColor from "../../../theme/variables/commonColor";
import { changeSideBarMenus } from '../../../actions/shared'
import NavigationService from '../../../service/NavigationService';

class Sidebar extends Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }

    navigateToSubPage(sideBarMenu) {
        this.props.closeDrawer();

        if (sideBarMenu === '<-') {
            NavigationService.navigate("Project List", null)
            return
        }

        if (this.props.activeTab == 'projects') {
            var formId = _.result(_.find(this.props.projectSidebar, function (form) {
                return form.name === sideBarMenu;
            }), 'form_id');
        } else {
            var formId = _.result(_.find(this.props.formsOrder, function (form) {
                return form.name === sideBarMenu;
            }), 'form_id');
        }

        if (formId && _.indexOf(["<-", "Forms Order", "Call Report", "Forms"], sideBarMenu) === -1 && _.indexOf([1, 2, 3, 7, 8, 12], formId) === -1) {
            NavigationService.navigate("StandardForm", {
                form_id: formId,
                project_id: this.props.projectId
            })
        } else {
            if (formId === 2) {
                NavigationService.navigate("Project Scope", {
                    form_id: formId,
                    project_id: this.props.projectId
                })
            } else {
                NavigationService.navigate(sideBarMenu, {
                    form_id: formId,
                    project_id: this.props.projectId
                })
            }
        }
    }

    render() {
        console.log(this.props.sideBarMenus)

        const listComponents = this.props.sideBarMenus.map((sideBarMenu) => (
            <ListItem key={sideBarMenu} noIndent button onPress={() => this.navigateToSubPage(sideBarMenu)}>
                <Left>
                    <Text>{sideBarMenu}</Text>
                </Left>
                <Right>
                    <Icon name="ios-arrow-forward" />
                </Right>
            </ListItem>
        ))
        return (
            <Content style={commonColor.sideBar}>
                <List>
                    {listComponents}
                </List>
            </Content>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        sideBarMenus: state.shared.sideBarMenus,
        formsOrder: state.standard_form.formsOrder,
        projectId: state.project.projectId,
        projectSidebar: state.project.projectSidebar
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        changeSideBarMenus: () => { dispatch(changeSideBarMenus()) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);