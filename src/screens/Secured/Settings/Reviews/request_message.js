import React, {
    Component
} from 'react';
import {
    connect
} from 'react-redux';
import {
    Content,
    Button,
    Icon,
    Text,
    List,
    Toast,
    Fab,
    CheckBox,
    ListItem,
    Body
} from "native-base";
import {
    ListView,
    View
} from 'react-native';
import styles from "./styles"
import {RichTextEditor, RichTextToolbar, actions} from 'react-native-zss-rich-text-editor';
import ImagePicker from 'react-native-image-picker'
import {
    changeHeaderTitle,    
    changeSideBarInivisble
} from '../../../../actions/shared'
import {
    handleErrorResponse
} from '../../../../service/error_handler'
import NavigationService from '../../../../service/NavigationService';
import BaseComponent from '../../BaseComponent'
import apiReviewRequestMessage from '../../../../api/review_request_message'

var company_id = -1;

const imgPickerOptions = {
    title: 'Select Image',
    storageOptions: {
        skipBackup: true,
        path: 'dryforms'
    },
    width: 150,
    height: 150,
    cropping: true,
};

class RequestMessageEdit extends BaseComponent {
    constructor(props) {
        super(props)
        this.state = {
            reviewRequestMessage: {
                company_id: null,
                message: ''
            }
        }
        this.changeContentText = this.changeContentText.bind(this);
        this.uploadImage = this.uploadImage.bind(this);
    }

    componentDidMount() {
        this.subs = [
            this.props.navigation.addListener("didFocus", () => {
                this.props.changeHeaderTitle("Review Request Message")
                this.props.invisableSideBar()
                console.log("review request message edit mounted")
                let params = this.props.navigation.state.params
                this.setState({
                    reviewRequestMessage: params.reviewRequestMessage
                })
                company_id = params.reviewRequestMessage.company_id
                this.loaded();
            }),
            this.props.navigation.addListener("willBlur", () => {})
        ];
    }

    componentWillUnmount() {
        this.subs.forEach(sub => sub.remove());
    }

    changeContentText(text) {
        apiReviewRequestMessage.patch(company_id, {
            company_id: company_id,
            message: text
        })
            .then((res) => {
            })
            .catch(error => {
                // handleErrorResponse(error)
            })
    }
    
    uploadImage() {
        ImagePicker.showImagePicker(imgPickerOptions, (response) => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                let source = 'data:image/jpeg;base64,' + response.data;
                this.richtext.insertImage({ src: source, width: 100, height: 100 });
            }
        });
    }

    async submit() {
        this.run()
        const contentHtml = await this.richtext.getContentHtml();
        this.setState((preState) => ({
            reviewRequestMessage: Object.assign({}, preState.reviewRequestMessage, { message: contentHtml })
        }), () => {
            apiReviewRequestMessage.patch(this.state.reviewRequestMessage.company_id, this.state.reviewRequestMessage)
                .then(() => {
                    Toast.show({
                        text: 'Review Request Message successfully updated',
                        duration: 3000,
                        type: "success"
                    });
                    this.dataReady()
                })
                .catch(error => {            
                    handleErrorResponse(error)
                    this.dataFailed()
                })
        });        
    }

    render() {
        return (
            this.isLoaded() ? (
                <View style={styles.container}>
                    <RichTextEditor
                        enableOnChange={ true }
                        ref={(r)=>{
                                this.richtext = r;
                                if (this.richtext) {
                                    this.richtext.registerContentChangeListener(this.changeContentText);
                                }
                            }}
                        style={styles.richText}
                        hiddenTitle= {true}
                        initialContentHTML={this.state.reviewRequestMessage.message}
                    />
                    <RichTextToolbar
                        getEditor={() => this.richtext}
                        actions={
                            [
                              actions.insertImage,
                              actions.setBold,
                              actions.setItalic,
                              actions.insertBulletsList,
                              actions.insertOrderedList,
                              actions.insertLink
                            ]
                        }
                        onPressAddImage={()=>{
                            this.uploadImage();
                        }}
                        iconTint='black'
                    />
                </View>
            ) : null
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user.user,
        company: state.user.company
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        changeHeaderTitle: (headerTitle) => {
            dispatch(changeHeaderTitle(headerTitle))
        },
        invisableSideBar: () => {
            dispatch(changeSideBarInivisble(true))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RequestMessageEdit);