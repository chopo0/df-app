import React, { Component } from "react";
import {
    Root
} from "native-base";
import {
    StackNavigator
} from "react-navigation";

import NewProject from "./New Project"
import ProjectList from "./Project List"
import FormCallReport from './Forms/CallReport'
// import ProjectScope from './Forms/Project Scope'

import NavigationService from '../../../service/NavigationService';

export const StackNav = StackNavigator({
    "New Project": {
        screen: NewProject,
        path: 'projects/new_project'
    },
    "Project List": {
        screen: ProjectList,
        path: 'projects/project_list'
    },
    "Call Report": {
        screen: FormCallReport,
        path: 'projects/form/call_report'
    }//,
    // "Project Scope": {
    //     screen: ProjectScope,
    //     path: 'projects/from/project_scope'
    // }
}, {
        index: 0,
        initialRouteName: "Project List",
        headerMode: "none"
    });

export default class Nav extends Component {
    render() {
        return (
            <Root>
                <StackNav
                    ref={navigatorRef => {
                        NavigationService.setTopLevelNavigator(navigatorRef);
                    }}
                />
            </Root>
        )
    }
}