import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Container, Header, Content, Form, List, View, Icon, Picker, Input, Label, Item, Button, Text, CheckBox, Body, ListItem, Title, Left, Right, Toast } from 'native-base';
import {
    TouchableOpacity, Alert
} from 'react-native'
import _ from 'lodash'
import { handleErrorResponse } from '../../../../../service/error_handler';
import { changeHeaderTitle, changeSideBarInivisble } from '../../../../../actions/shared';
import NavigationService from '../../../../../service/NavigationService';
import apiStatus from '../../../../../api/status';
import apiTeams from '../../../../../api/teams';
import apiCategory from '../../../../../api/categories';
import apiModels from '../../../../../api/models';
import apiEquipments from '../../../../../api/equipment';
import BaseComponent from '../../../BaseComponent';
import styles from '../styles'

const reasons = [
    'Please Input Serial Number',
    'Please input valid category',
    'Serial number alreday exists',
    'Serial number is not numeric',
    'Please check network state'
]

export default class EquipmentNumbers extends BaseComponent {
    constructor(props) {
        super(props);
        this.state = {
            visibleModal: false,
            serials: [],
            currentPage: "1",
            perPage: 10,
            count: 0
        };
        this.goPage = _.debounce(this.goPage.bind(this), 100);
        this.validateSerialNumber = _.debounce(this.validateSerialNumber.bind(this), 300);

        this.goPageImediately = this.goPageImediately.bind(this)
        this.getSerials = this.getSerials.bind(this)
        this.setSerial = this.setSerial.bind(this)
        this.validateSerialNumberIme = this.validateSerialNumberIme.bind(this)
    }

    componentDidMount() {
        let serials = []
        for (let i = 0; i < this.props.quantity; i++) {
            if (this.props.serials[i] && this.props.serials[i].value) {
                serials[i] = this.props.serials[i]
            } else {
                serials[i] = {
                    key: i,
                    value: "",
                    validate: false,
                    reason: reasons[0]
                }
            }
        }
        this.setState({
            serials: serials,
            count: Math.ceil(Number(this.props.quantity) / this.state.perPage)
        })
    }

    goPageImediately(pageNum) {
        pageNum = pageNum || 1;
        pageNum = (pageNum > this.state.count) ? this.state.count : pageNum
        pageNum = "" + pageNum
        this.setState({
            currentPage: pageNum
        }, () => {
            this.getSerials();
        })
    }

    goPage() {
        this.getSerials();
    }

    getSerials() {
    }

    setSerial(value, serial) {
        serial.value = value
        this.setState({
            serials: this.state.serials
        }, () => {
            this.validateSerialNumber(serial)
        })
    }

    validateSerialNumber(serial) {
        this.validateSerialNumberIme(serial)
    }

    validateSerialNumberIme(serial) {
        if (!serial.value) {
            serial.validate = false
            serial.reason = reasons[0]
            this.resetSerialNumbers()
            return false
        }

        if (!this.props.category_id && (!this.props.newCategory || !this.props.newPrefix)) {
            serial.validate = false
            serial.reason = reasons[1]
            this.resetSerialNumbers()
            return false
        }

        if (!this.props.category_id && (this.props.newCategory && this.props.newPrefix)) {
            if (this.state.categories.some(category => category.name === this.props.newCategory)) {
                serial.validate = false
                serial.reason = reasons[1]
                this.resetSerialNumbers()
                return false
            }
            serial.validate = true
            serial.reason = ""
            return true
        }
        return apiEquipments.valdiateSerial(serial.value, this.props.category_id)
            .then(response => {
                if (response.data.message === 'exist') {
                    serial.validate = false
                    serial.reason = reasons[2]
                    this.resetSerialNumbers()
                    return false;
                }
                if (response.data.message === 'serial is not numeric') {
                    serial.validate = false
                    serial.reason = reasons[3]
                    this.resetSerialNumbers()
                    return false
                }
                serial.validate = true
                serial.reason = ""
                this.resetSerialNumbers()
                return false
            }).catch(err => {
                if (err) {
                    serial.validate = false
                    serial.reason = reasons[4]
                    this.resetEquipment(equipment)
                    return false
                }
            })
    }

    warning(serial) {
        Alert.alert(
            serial.reason
        )
    }

    resetSerialNumbers() {
        this.setState({
            serials: this.state.serials
        })
    }

    save() {
        for (let index = 0; index < this.state.serials.length; index++) {
            const serial = this.state.serials[index];
            if (!serial.validate) {
                Alert.alert("Please enter valid serial numbers")
                return
            }
        }
        this.props.addEquip(this.state.serials)
        this.props.dismissModal()
    }

    render() {
        let startPos = (Number(this.state.currentPage) - 1) * Number(this.state.perPage)
        let endPos = Math.min(startPos + Number(this.state.perPage), this.state.serials.length)
        let serials = this.state.serials.slice(startPos, endPos)

        return (
            <Container style={styles.dlgBg}>
                <Header>
                    <Left>
                        <Button transparent onPress={() => {
                            this.props.dismissModal();
                        }}>
                            <Icon name="md-arrow-round-back" />
                        </Button>
                    </Left>
                    <Body style={{ justifyContent: 'flex-start' }}>
                        <Title style={{ fontSize: 15, textAlign: 'left' }}>Edit Equipment Numbers</Title>
                    </Body>
                    <Right>
                        <Button transparent onPress={() => { this.save() }}>
                            <Icon name="md-checkmark" />
                        </Button>
                    </Right>
                </Header>
                <View style={{ justifyContent: 'center', alignItems: 'center', paddingVertical: 10 }}>
                    <Item noBorder style={{ width: 300 }}>
                        <Button small block info style={styles.navBtn}
                            onPress={() => this.goPageImediately(1)}
                            disabled={parseInt(this.state.currentPage) <= 1}
                        >
                            <Text style={{ textAlign: 'center' }}> {'<<'} </Text>
                        </Button>
                        <Button small block info style={styles.navBtn}
                            onPress={() => this.goPageImediately(parseInt(this.state.currentPage) - 1)}
                            disabled={parseInt(this.state.currentPage) <= 1}
                        >
                            <Text> {'<'} </Text>
                        </Button>
                        <Input keyboardType='numeric' style={{ fontSize: 15, paddingVertical: 0, flex: 1.5, textAlign: 'center', height: 32, backgroundColor: '#28B8F6', color: 'white' }} value={this.state.currentPage} onChangeText={(text) => {
                            let pageNum = parseInt(text) || 1;
                            pageNum = (pageNum > this.state.count) ? this.state.count : pageNum
                            pageNum = "" + pageNum
                            this.setState({
                                currentPage: pageNum
                            })
                            this.goPage()
                        }}></Input>
                        <Button small block info style={styles.navBtn}
                            onPress={() => this.goPageImediately(parseInt(this.state.currentPage) + 1)}
                            disabled={parseInt(this.state.currentPage) >= this.state.count}
                        >
                            <Text> {'>'} </Text>
                        </Button>
                        <Button small block info style={styles.navBtn}
                            onPress={() => this.goPageImediately(this.state.count)}
                            disabled={parseInt(this.state.currentPage) >= this.state.count}
                        >
                            <Text> {'>>'} </Text>
                        </Button>
                    </Item>
                </View>
                <Content>
                    <View style={{ borderColor: 'white', borderWidth: 2 }}>
                        {
                            (serials.length > 0) ?
                                (serials.map(serial => {
                                    return (
                                        <Item error={!serial.validate} style={{ paddingVertical: 0, backgroundColor: serial.key % 2 == 1 ? '#ffffff' : '#f2f2f2' }}>
                                            <Input keyboardType='numeric' value={serial.value} style={{ paddingVertical: 0 }} onChangeText={(value) => { this.setSerial(value, serial) }}></Input>
                                            {
                                                (!serial.validate) ? (
                                                    <TouchableOpacity onPress={() => { this.warning(serial) }} style={[styles.validate, { backgroundColor: '#fa8072' }]} ><Text><Icon name="md-alert" style={{ color: 'white' }}></Icon></Text></TouchableOpacity>) : null
                                            }
                                        </Item>
                                    )
                                })) : null
                        }
                    </View>
                </Content>
            </Container>
        );
    }
}