import React from 'react';
import { connect } from 'react-redux';
import { Container, Content, Item, Input, List, Icon, SwipeRow, Body, Text, Button, Fab } from 'native-base';
import { View, Alert } from 'react-native';
import _ from 'lodash'
import { handleErrorResponse } from '../../../../service/error_handler';
import { changeHeaderTitle, changeSideBarInivisble } from '../../../../actions/shared';
import Search from 'react-native-search-box';
import apiCategories from '../../../../api/categories';
import NavigationService from '../../../../service/NavigationService';
import BaseComponent from '../../BaseComponent';
import LoadingComponent from '../../BaseComponent/loading'
import styles from "./styles"

class Categories extends BaseComponent {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            pageNumber: 1,
            selectedDeleteId: 0,
            deleteDialogvisible: false,
            isOpen: false,
            isDisabled: false,
            swipeToClose: false,
            modelOpen: false,
            modalId: 0,
            existingCategory: "",
            existingPrefix: "",
            description: null,
            company_id: 0,
            addModalVisible: false,
            addModalName: "",
            addModalPrefix: "",


            categories: [],
            modal: null,
            currentPage: "1",
            perPage: 10,
            count: 0,
            sortBy: '',
            sortDesc: false,
            filter: ''
        }
        this.filter = _.debounce(this.filter.bind(this), 300);
        this.goPage = _.debounce(this.goPage.bind(this), 300);

        this.goPageImediately = this.goPageImediately.bind(this)
    }


    componentDidMount() {
        this.subs = [
            this.props.navigation.addListener("didFocus", () => {
                this.props.changeHeaderTitle("Manage Equipment categories");
                this.props.visableSideBar();
                this.getCategories();
                console.log("Equipments Categories mounted")
            }),
            this.props.navigation.addListener("willBlur", () => {

            })
        ]
    }

    componentWillUnmount() {
        this.subs.forEach(sub => sub.remove());
    }

    getCategories() {
        this.run()
        let data = {
            page: this.state.currentPage,
            sort_by: this.state.sortBy || '',
            sort_type: (this.state.sortDesc ? 'desc' : 'asc'),
            per_page: this.state.perPage,
            filter: this.state.filter
        }
        return apiCategories.index(data)
            .then(response => {
                this.setState({
                    categories: response.data.data || [],
                    count: Math.ceil(response.data.total / this.state.perPage)
                })
                this.dataReady()
                this.loaded()
            })
            .catch(err => {
                handleErrorResponse(err)
            });
    };

    filter(text) {
        this.setState({
            filter: text
        }, () => {
            this.getCategories();
        })
    }

    goPageImediately(pageNum) {
        pageNum = pageNum || 1;
        pageNum = (pageNum > this.state.count) ? this.state.count : pageNum
        pageNum = "" + pageNum
        this.setState({
            currentPage: pageNum
        }, () => {
            this.getCategories();
        })
    }

    goPage() {
        this.getCategories();
    }

    editCategory(id) {
        NavigationService.navigate('CategoriesEdit', {
            id: id,
            action: 'edit'
        })
    }

    addCategory() {
        NavigationService.navigate('CategoriesEdit', {
            action: 'create'
        })
    }

    removeCategory(id) {
        Alert.alert(
            'Delete Category Confirmation',
            'Are you sure?',
            [
                { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                {
                    text: 'Confirm', onPress: () => {
                        apiCategories.delete(id)
                            .then(() => {
                                    this.getCategories();
                                })
                            .catch(handleErrorResponse)
                    }
                },
            ],
            { cancelable: false }
        )
    }

    render() {
        let count = this.state.count;
        return (
            <Container>
                <View style={{}}>
                    <Search
                        ref="filter_box"
                        placeholder="Categories"
                        afterFocus={() => {
                            console.log("After focus");
                        }}
                        onChangeText={text => {
                            this.filter(text)
                        }}
                        onDelete={() => {
                            this.filter("")
                        }}
                        onCancel={() => {
                            this.filter("")
                        }}
                    />
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center', marginVertical: 10 }}>
                    <Item style={{ width: 300 }}>
                        <Button small block info style={styles.navBtn}
                            onPress={() => this.goPageImediately(1)}
                            disabled={Number(this.state.currentPage) <= 1}
                        >
                            <Text style={{ textAlign: 'center' }}> {'<<'} </Text>
                        </Button>
                        <Button small block info style={styles.navBtn}
                            onPress={() => this.goPageImediately(Number(this.state.currentPage) - 1)}
                            disabled={Number(this.state.currentPage) <= 1}
                        >
                            <Text> {'<'} </Text>
                        </Button>
                        <Input keyboardType='numeric' style={{ fontSize: 15, paddingVertical: 0, flex: 1.5, textAlign: 'center', height: 32, backgroundColor: '#28B8F6', color: 'white' }} value={this.state.currentPage} onChangeText={(text) => {
                            let pageNum = Number(text) || 1;
                            pageNum = (pageNum > count) ? count : pageNum
                            pageNum = "" + pageNum
                            this.setState({
                                currentPage: pageNum
                            })
                            this.goPage()
                        }}></Input>
                        <Button small block info style={styles.navBtn}
                            onPress={() => this.goPageImediately(Number(this.state.currentPage) + 1)}
                            disabled={Number(this.state.currentPage) >= this.state.count}
                        >
                            <Text> {'>'} </Text>
                        </Button>
                        <Button small block info style={styles.navBtn}
                            onPress={() => this.goPageImediately(this.state.count)}
                            disabled={Number(this.state.currentPage) >= this.state.count}
                        >
                            <Text> {'>>'} </Text>
                        </Button>
                    </Item>
                </View>
                <Content padder>
                    {!this.state.isRunning ? (
                        <List>
                            {
                                (this.state.categories.length > 0) ?
                                    this.state.categories.map((category) => {
                                        return (
                                            <SwipeRow
                                                leftOpenValue={75}
                                                rightOpenValue={-75}
                                                left={
                                                    <Button success onPress={() => { this.editCategory(category.id) }}>
                                                        <Icon name="create" />
                                                    </Button>
                                                }
                                                body={
                                                    <Body style={{ flexDirection: 'row', paddingVertical: 0 }}>
                                                        <View style={[{ flex: 2 }]}>
                                                            <Text style={{}} numberOfLines={1}>
                                                                {category.name}
                                                            </Text>
                                                        </View>
                                                        <View style={[styles.defaultText, { flex: 1 }]}>
                                                            <Text style={{}} numberOfLines={1}>
                                                                {category.prefix}
                                                            </Text>
                                                        </View>
                                                    </Body>
                                                }
                                                right={
                                                    <Button danger onPress={() => { this.removeCategory(category.id) }}>
                                                        <Icon active name="trash" />
                                                    </Button>
                                                }
                                            />
                                        )
                                    }) : null
                            }
                        </List>
                    ) : (
                            <LoadingComponent />
                        )
                    }
                </Content>
                <Fab
                    style={{ backgroundColor: '#5067FF' }}
                    position="bottomRight"
                    onPress={() => {
                        this.addCategory()
                    }}>
                    <Icon name="add" />
                </Fab>
            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        changeHeaderTitle: (headerTitle) => {
            dispatch(changeHeaderTitle(headerTitle))
        },
        visableSideBar: () => {
            dispatch(changeSideBarInivisble(false))
        },
    }
}

export default connect(undefined, mapDispatchToProps)(Categories);
