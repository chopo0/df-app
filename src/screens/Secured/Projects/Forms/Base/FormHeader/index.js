import React from 'react';
import {
    connect
} from 'react-redux';
import { Container, Grid, Content, Item, List, ListItem, Body, Picker, Text, Button, Fab, Badge, Icon, Row, Col, Spinner, Input, Header, Thumbnail, CheckBox } from 'native-base';
import {
    ListView,
    FlatList,
    Image,
    View
} from 'react-native';
import _ from 'lodash'
import { Alert } from 'react-native'
import EventEmitter from 'EventEmitter';
import shallowCompare from 'react-addons-shallow-compare';

import NavigationService from '../../../../../../service/NavigationService'
import {
    changeHeaderTitle,
    changeSideBarInivisble
} from '../../../../../../actions/shared'
import {
    handleErrorResponse
} from '../../../../../../service/error_handler'
import BaseComponent from '../../../../BaseComponent'
import styles from "./styles"
import apiFormsOrder from '../../../../../../api/forms_order'
import { fetchFormsOrder } from '../../../../../../service/standard_form'
import {
    fetchUser
} from '../../../../../../service/fetch_user'
import LoadingComponent from '../../../../BaseComponent/loading'
import {
    API_DOMAIN
} from '../../../../../../config/env'

class FormHeader extends BaseComponent {
    constructor(props) {
        super(props)

        this.state = {
        }
    }

    componentDidMount() {
    }

    componentWillUnmount() {
    }

    getImageUri(img) {
        if (img.indexOf('data:image/jpeg') !== -1 || img.indexOf('content://') !== -1) {
            return img;
        }
        let link = API_DOMAIN + '/' + img;
        return link
    }

    render() {
        return (
            <Content>
                {this.props.companyDetails ? (
                    <Grid style={{ borderBottomColor: 'grey', borderBottomWidth: 2, paddingVertical: 10 }}>
                        <Col>
                            {this.props.companyDetails.logo ? (
                                <Image resizeMode="contain" style={{ flex: 1 }} source={{ uri: this.getImageUri(this.props.companyDetails.public_logo_path) }} />
                            ) : (
                                    <Image resizeMode="contain" style={{ flex: 1 }} source={require('../../../../../../../assets/fallback-logo.jpg')} />
                                )}
                        </Col>
                        <Col>
                            <Text>{this.props.companyDetails.name ? this.props.companyDetails.name : ''}</Text>
                            <Text>{this.props.companyDetails.street ? this.props.companyDetails.street : ''}</Text>
                            <Text>{this.props.companyDetails.city ? this.props.companyDetails.city : ''} {this.props.companyDetails.state ? this.props.companyDetails.state : ''} {this.props.companyDetails.zip ? this.props.companyDetails.zip : ''}</Text>
                            <Text>{this.props.companyDetails.phone ? this.props.companyDetails.phone : ''}</Text>
                            <Text>{this.props.companyDetails.email ? this.props.companyDetails.email : ''}</Text>
                        </Col>
                    </Grid>
                ) : null}
            </Content>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user.user,
        company: state.user.company,
        companyDetails: state.user.companyDetails
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        changeHeaderTitle: (headerTitle) => {
            dispatch(changeHeaderTitle(headerTitle))
        },
        visableSideBar: () => {
            dispatch(changeSideBarInivisble(false))
        },
        fetchUser: () => {
            dispatch(fetchUser())
        },
        fetchFormsOrder: () => {
            dispatch(fetchFormsOrder())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FormHeader);