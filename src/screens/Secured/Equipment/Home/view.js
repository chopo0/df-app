import React from 'react';
var RN = require("react-native");
import { connect } from 'react-redux';
import { View } from 'react-native';
import { Container, Content, Item, List, ListItem, Body, Picker, Text, Button, Icon, Row, Col, Input, Label } from 'native-base';
import {
	TouchableOpacity, Alert
} from 'react-native'
import Search from 'react-native-search-box';
import _ from 'lodash'
import moment from 'moment'

import apiEquipment from '../../../../api/equipment';
import apiTeams from '../../../../api/teams';
import apiEquipmentLocations from '../../../../api/equipment-locations'
import { changeHeaderTitle, changeSideBarInivisble } from '../../../../actions/shared';
import BaseComponent from '../../BaseComponent';
import LoadingComponent from '../../BaseComponent/loading'
import styles from "./styles"
import NavigationService from '../../../../service/NavigationService';
import {
	handleErrorResponse
} from '../../../../service/error_handler'

var {
	Dimensions
} = RN

class ViewEquipments extends BaseComponent {

	constructor(props) {
		super(props);
		this.state = {
			isLoaded: false,
			model: {},
			equipments: [],
			headerText: '',
			currentPage: "1",
			perPage: 10,
			count: 0,
			sortBy: '',
			sortDesc: false,
			filter: '',
			fiter_debouncer: '',
			statuses: [],
			locations: [],
			teams: [{
				name: '------'
			}],
			locationDirty: false,
			serialDirty: false,
		};


		this.filter = _.debounce(this.filter.bind(this), 300);
		this.goPage = _.debounce(this.goPage.bind(this), 300);
		this.validateSerialNumber = _.debounce(this.validateSerialNumber.bind(this), 300);
		this.validateSerialNumberIme = this.validateSerialNumberIme.bind(this)

		this.goPageImediately = this.goPageImediately.bind(this)
		this.getEquipments = this.getEquipments.bind(this)
		this.changeLoanLocation = this.changeLoanLocation.bind(this)
		this.changeEquipmentNumber = this.changeEquipmentNumber.bind(this)
		this.saveEquipmentNumber = this.saveEquipmentNumber.bind(this)
		this.save = this.save.bind(this)
	}

	componentDidMount() {
		this.subs = [
			this.props.navigation.addListener("didFocus", () => {
				let params = this.props.navigation.state.params
				this.setState({
					model: params.model,
					statuses: params.statuses
				}, () => {
					this.initData()
				})
				this.props.invisableSideBar();
				console.log("View equipments mounted");
			})
		];
	}

	componentWillUnmount() {
		this.subs.forEach(sub => sub.remove());
	}

	initData() {
		this.setState({
			isLoaded: false
		}, () => {
			const apis = [
				apiTeams.index(),
				apiEquipmentLocations.index()
			]
			return Promise.all(apis)
				.then(response => {
					let teams = [{
						name: '------'
					}].concat(response[0].data.data)
					let locations = response[1].data
					this.setState({
						teams: teams,
						locations: locations,
						locationDirty: false,
						serialDirty: false
					})
					this.getEquipments()
				})
		})
	}

	getEquipments() {
		this.run()
		let data = {
			category_id: this.state.model.category_id || '',
			model_id: this.state.model.id || '',
			status_id: this.state.model.status || '',
			page: this.state.currentPage,
			sort_by: this.state.sortBy || 'serial',
			sort_type: (this.state.sortDesc ? 'desc' : 'asc'),
			per_page: this.state.perPage,
			filter: this.state.filter,
			id_from: ''
		}
		return apiEquipment.index(data)
			.then(response => {
				let items = response.data.data
				items = items.map(item => {
					item.model = item.model || { name: '' }
					item.model.category = item.model.category || { name: '' }
					item.status = item.status || { name: '' }
					item.team = item.team || { name: '' }
					let number = item.serial.replace(item.model.category.prefix + ' ', '')
					item.serial = {
						original: item.serial,
						prefix: item.model.category.prefix,
						number: number,
						dirty: false,
						validate: true
					}
					return item
				})
				let headerText = (response.data.data.length) ? response.data.data[0].model.category.name : ''
				if (this.state.model.id && response.data.data.length) {
					headerText += ' / ' + response.data.data[0].model.name
					if (this.state.model.status) {
						headerText += ' / ' + response.data.data[0].status.name
					}
				}
				this.props.changeHeaderTitle(headerText)
				this.setState({
					equipments: items || [],
					count: Math.ceil(response.data.total / this.state.perPage),
				})
				this.dataReady()
				this.loaded()
			})
	}

	filter(text) {
		this.setState({
			filter: text
		})
		this.getEquipments();
	}

	goPageImediately(pageNum) {
		pageNum = pageNum || 1;
		pageNum = (pageNum > this.state.count) ? this.state.count : pageNum
		pageNum = "" + pageNum
		this.setState({
			currentPage: pageNum
		}, () => {
			this.getEquipments();
		})
	}

	goPage() {
		this.getEquipments();
	}

	changeTeam(equipment, teamId) {
		let data = _.cloneDeep(equipment)
		data.team_id = teamId || ''
		this.save(data)
	}

	changeStatus(equipment, statusId) {
		let data = _.cloneDeep(equipment)
		data.status_id = statusId
		this.save(data)
	}

	changeLocation(equipment, project_id) {
		let data = _.cloneDeep(equipment)
		let location = _.find(this.state.locations, function (obj) {
			return obj.id === project_id;
		});
		data.location = location.location
		data.project_id = project_id
		this.save(data)
	}
	changeLoanLocation(equipment, location) {
		equipment.location = location
		equipment.locationDirty = true
		this.setState({
			equipments: this.state.equipments
		})
	}
	changeEquipmentNumber(equipment, number) {
		equipment.serial.number = number
		equipment.serial.dirty = true
		this.resetEquipments()
		this.validateSerialNumber(equipment)
	}
	validateSerialNumber(equipment) {
		this.validateSerialNumberIme(equipment)
	}
	validateSerialNumberIme(equipment) {
		if (!equipment.serial.number) {
			equipment.serial.validate = false
			this.resetEquipments()
			return false
		}
		if (equipment.serial.original === equipment.serial.prefix + ' ' + equipment.serial.number) {
			equipment.serial.validate = true
			this.resetEquipments()
			return true
		}
		let serial = equipment.serial.number
		return apiEquipment.valdiateSerial(serial, equipment.model.category_id)
			.then(response => {
				if (response.data.message === 'nonexistence') {
					equipment.serial.validate = true
					this.resetEquipments()
					return true;
				}
				equipment.serial.validate = false
				this.resetEquipments()
				return false
			}).catch(err => {
				if (err) {
					equipment.serial.validate = false
					this.resetEquipments()
					return false
				}
			})
	}
	resetEquipments() {
		this.setState({
			equipments: this.state.equipments
		})
	}
	async saveEquipmentNumber(equipment) {
		let isSerialNumberValidate = await this.validateSerialNumberIme(equipment)
		if (isSerialNumberValidate) {
			this.save(equipment)
		}
	}
	save(equipment) {
		let data = _.cloneDeep(equipment)
		data.serial = equipment.serial.number
		data.date = moment().format('YYYY-MM-DD')
		apiEquipment.patch(data.id, data)
			.then(() => {
				this.initData();
			})
			.catch(handleErrorResponse)
	}
	removeEquipment(id) {
		Alert.alert(
			'Delete Equipment Confirmation',
			'Are you sure?',
			[
				{ text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
				{
					text: 'Confirm', onPress: () => {
						apiEquipment.delete(id)
							.then(() => {
								this.initData();
							})
							.catch(error => {
								console.log(error.data)
							})
					}
				},
			],
			{ cancelable: false }
		)
	}
	editEquipment(equipment) {
		NavigationService.navigate('EditEquipment', {
			id: equipment.id,
			category_id: equipment.model.category_id,
			statuses: this.state.statuses,
			teams: this.state.teams
		})
	}

	render() {
		const width = Dimensions.get('window').width
		let statusesPickerItems = this.state.statuses && this.state.statuses.map((s, i) => {
			return <Picker.Item key={i} value={s.value} label={s.text} />
		})
		let teamsPickerItems = this.state.teams && this.state.teams.map((t, i) => {
			return <Picker.Item key={i} value={t.id} label={t.name} />
		})
		let locationsPickerItems = this.state.locations && this.state.locations.map((l, i) => {
			return <Picker.Item key={i} value={l.id} label={l.location} />
		})
		let count = this.state.count;
		let index = 0;
		return (
			<Container>
				<View>
					<Search
						ref="view_search"
						placeholder="Type text..."
						afterFocus={() => {
							console.log("After focus");
						}}
						onChangeText={text => {
							this.filter(text)
						}}
						onCancel={() => {
							this.filter("")
						}}
						onDelete={() => {
							this.filter("")
						}}
					/>
				</View>
				<View style={{ justifyContent: 'center', alignItems: 'center', marginVertical: 10 }}>
					<Item style={{ width: 300 }}>
						<Button small block info style={styles.navBtn}
							onPress={() => this.goPageImediately(1)}
							disabled={parseInt(this.state.currentPage) <= 1}
						>
							<Text style={{ textAlign: 'center' }}> {'<<'} </Text>
						</Button>
						<Button small block info style={styles.navBtn}
							onPress={() => this.goPageImediately(parseInt(this.state.currentPage) - 1)}
							disabled={parseInt(this.state.currentPage) <= 1}
						>
							<Text> {'<'} </Text>
						</Button>
						<Input keyboardType='numeric' style={{ fontSize: 15, paddingVertical: 0, flex: 1.5, textAlign: 'center', height: 32, backgroundColor: '#28B8F6', color: 'white' }} value={this.state.currentPage} onChangeText={(text) => {
							let pageNum = parseInt(text) || 1;
							pageNum = (pageNum > count) ? count : pageNum
							pageNum = "" + pageNum
							this.setState({
								currentPage: pageNum
							})
							this.goPage()
						}}></Input>
						<Button small block info style={styles.navBtn}
							onPress={() => this.goPageImediately(parseInt(this.state.currentPage) + 1)}
							disabled={parseInt(this.state.currentPage) >= this.state.count}
						>
							<Text> {'>'} </Text>
						</Button>
						<Button small block info style={styles.navBtn}
							onPress={() => this.goPageImediately(this.state.count)}
							disabled={parseInt(this.state.currentPage) >= this.state.count}
						>
							<Text> {'>>'} </Text>
						</Button>
					</Item>
				</View>
				<Content style={{ backgroundColor: 'white' }}>
					{this.state.isLoaded && !this.state.isRunning ? (
						<List>
							{
								(this.state.equipments.length > 0) ?
									(this.state.equipments.map(equipment => {
										index++
										return (
											<React.Fragment key={index}>
												<ListItem noIndent style={{ borderBottomColor: '#eee', borderBottomWidth: 1, paddingVertical: 3, backgroundColor: index % 2 == 1 ? '#ffffff' : '#f2f2f2' }}>
													<Body style={styles.body}>
														<Row>
															<Col style={{ justifyContent: 'flex-start', flexDirection: 'row', marginVertical: 10, borderWidth: 0 }}>
																<Label style={{
																	fontWeight: 'bold'
																}}>
																	Category : </Label>
																<Text style={{
																}} numberOfLines={1}>
																	{equipment.model.category.name ? equipment.model.category.name : 'n/a'}
																</Text>
															</Col>
															<Col style={{ justifyContent: 'flex-start', flexDirection: 'row', marginVertical: 10, borderWidth: 0 }}>
																<Label style={{
																	fontWeight: 'bold'
																}}>
																	Make / Model : </Label>
																<Text style={{
																	width: width / 2 - 150
																}} numberOfLines={1}>
																	{equipment.model.name}
																</Text>
															</Col>
														</Row>
														<View style={{ flexDirection: 'row', marginVertical: 10, borderWidth: 0, width: '100%' }}>
															<Picker
																mode="dropdown"
																iosIcon={<Icon name="ios-arrow-down-outline" />}
																placeholder="Select Team"
																placeholderStyle={{ color: "#bfc6ea", }}
																placeholderIconColor="#007aff"
																style={[styles.picker1, { flex: 1, marginRight: 5 }]}
																selectedValue={equipment.team.id}
																onValueChange={(value) => this.changeTeam(equipment, value)}
															>
																{teamsPickerItems}
															</Picker>
															{equipment.status_id === 4 ? (
																<View style={{ flex: 1, flexDirection: 'row', marginRight: 5 }}>
																	<Input style={[styles.picker2, { paddingVertical: 0 }]} placeholder="Location" placeholderTextColor={"#aaa"} value={equipment.location} onChangeText={(text) => this.changeLoanLocation(equipment, text)}></Input>
																	{
																		(equipment.locationDirty) ? (
																			<View style={{}}>
																				<Button onPress={() => this.save(equipment)} block info style={[styles.location, {}]} ><Icon name="md-checkmark" style={{}}></Icon></Button>
																			</View>) : null
																	}
																</View>
															) : (
																	<Picker
																		mode="dropdown"
																		iosIcon={<Icon name="ios-arrow-down-outline" />}
																		placeholder="Select Location"
																		placeholderStyle={{ color: "#bfc6ea" }}
																		placeholderIconColor="#007aff"
																		style={[styles.picker2, { flex: 1, marginRight: 5 }]}
																		selectedValue={equipment.project_id}
																		onValueChange={(value) => this.changeLocation(equipment, value)}
																	>
																		{locationsPickerItems}
																	</Picker>
																)
															}
															<Picker
																mode="dropdown"
																iosIcon={<Icon name="ios-arrow-down-outline" />}
																placeholder="Select Status"
																placeholderStyle={{ color: "#bfc6ea" }}
																placeholderIconColor="#007aff"
																style={[styles.picker1, { flex: 1 }]}
																selectedValue={equipment.status_id}
																onValueChange={(value) => this.changeStatus(equipment, value)}
															>
																{statusesPickerItems}
															</Picker>
														</View>
														<Item inlineLabel>
															<Label style={{ fontWeight: 'bold' }} numberOfLines={1}>Equipment# : </Label>
															<Text style={{}} numberOfLines={1}>{equipment.serial.prefix}</Text>
															<Input keyboardType='numeric' style={{ paddingVertical: 0, height: 32 }} value={equipment.serial.number} onChangeText={(text) => { this.changeEquipmentNumber(equipment, text) }}></Input>
															{
																(!equipment.serial.validate) ? (
																	<TouchableOpacity onPress={() => { }} style={[styles.validate, { backgroundColor: '#fa8072' }]} ><Text><Icon name="md-alert" style={{ color: 'white' }}></Icon></Text></TouchableOpacity>) : null
															}
															{
																(equipment.serial.validate && equipment.serial.dirty) ? (
																	<TouchableOpacity onPress={() => this.saveEquipmentNumber(equipment)} style={[styles.validate, { backgroundColor: '#28B8F6' }]}>
																		<Text><Icon name="md-checkmark" style={{ color: 'white' }}></Icon></Text>
																	</TouchableOpacity>) : null
															}
														</Item>
														<View style={{ justifyContent: 'flex-end', flexDirection: 'row', marginVertical: 10, borderWidth: 0, width: '100%' }}>
															<Button small info style={{ marginRight: 10 }}
																onPress={() => { this.editEquipment(equipment) }}
															>
																<Icon active name="md-create" />
															</Button>
															<Button small danger style={{ marginLeft: 10 }}
																onPress={() => { this.removeEquipment(equipment.id) }}
															>
																<Icon active name="trash" />
															</Button>
														</View>
													</Body>
												</ListItem>
											</React.Fragment>
										)
									}))
									: null
							}
						</List>
					) : (
							<LoadingComponent />
						)
					}
				</Content>
			</Container>
		);
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		changeHeaderTitle: (headerTitle) => {
			dispatch(changeHeaderTitle(headerTitle))
		},
		invisableSideBar: () => {
			dispatch(changeSideBarInivisble(true))
		},
	}
}

export default connect(undefined, mapDispatchToProps)(ViewEquipments);
