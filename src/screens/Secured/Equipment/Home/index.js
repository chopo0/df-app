import React from 'react';
import { connect } from 'react-redux';
import { Container, Content, Item, List, ListItem, Body, Picker, Text, Button, Fab, Badge, Icon, Row, Col, Spinner, Input } from 'native-base';
import { View } from 'react-native';
import { changeHeaderTitle, changeSideBarInivisble } from '../../../../actions/shared';
import Search from 'react-native-search-box';
import apiModels from '../../../../api/models';
import apiStatuses from '../../../../api/status';
import NavigationService from '../../../../service/NavigationService';
import BaseComponent from '../../BaseComponent';
import styles from "./styles"
import _ from 'lodash'

class Home extends BaseComponent {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            models: [],
            statuses: [],
            currentPage: "1",
            perPage: 10,
            count: 0,
            sortBy: 'category_name',
            sortDesc: false,
            filter_category: '',
            filter_model: '',
            filter: ''
        }

        this.filter = _.debounce(this.filter.bind(this), 300);
        this.goPage = _.debounce(this.goPage.bind(this), 300);
        this.goPageImediately = this.goPageImediately.bind(this)
    };

    componentDidMount() {
        this.subs = [
            this.props.navigation.addListener("didFocus", () => {
                this.props.changeHeaderTitle("Manage your Inventory");
                this.props.visableSideBar();
                this.initData();
                console.log("Equipments home mounted");
            }),
            this.props.navigation.addListener("willBlur", () => { })
        ];
    }

    componentWillUnmount() {
        this.subs.forEach(sub => sub.remove());
    }

    initData() {
        this.setState({
            isLoaded: false
        }, () => {
            return apiStatuses.index()
                .then(response => {
                    let statuses = [{ value: null, text: 'All Status' }]
                    for (let status of response.data.data) {
                        statuses.push({
                            value: status.id,
                            text: status.name
                        })
                    }
                    this.setState({
                        statuses: statuses
                    });
                    this.getModels();
                });
        })
    }

    getModels() {
        this.run()
        let data = {
            page: this.state.currentPage,
            sort_by: this.state.sortBy || '',
            sort_type: (this.state.sortDesc ? 'desc' : 'asc'),
            category_name: this.state.filter_category || '',
            model_name: this.state.filter_model || '',
            per_page: this.state.perPage
        }
        return apiModels.index(data)
            .then(response => {
                let items = response.data.data
                items = items.map(item => {
                    item.status = 1
                    return item
                })
                this.setState({
                    count: Math.ceil(response.data.total / this.state.perPage),
                    models: items || []
                });
                this.loaded()
                this.dataReady()
            })
    }

    filter(field, text) {
        if (field === 'cat') {
            this.setState({
                filter_category: text
            })
        } else if (field === 'model') {
            this.setState({
                filter_model: text
            })
        }
        this.getModels();
    }

    goPageImediately(pageNum) {
        pageNum = pageNum || 1;
        pageNum = (pageNum > this.state.count) ? this.state.count : pageNum
        pageNum = "" + pageNum
        this.setState({
            currentPage: pageNum
        }, () => {
            this.getModels();
        })
    }

    goPage() {
        this.getModels();
    }

    countPerStatus(item) {
        if (!item.status) {
            return item.total
        }
        let count = item['status_' + item.status + '_count']
        return count
    }

    onStatusChange(value, model) {
        let models = this.state.models;
        model.status = value
        this.setState({
            models: models
        })
    }

    viewEquipments(model) {
        NavigationService.navigate('ViewEquipments', {
            model: model,
            statuses: this.state.statuses
        })
    }

    render() {
        let curDevider = '';
        let statusesPickerItems = this.state.statuses && this.state.statuses.map((s, i) => {
            return <Picker.Item key={i} value={s.value} label={s.text} />
        })
        let count = this.state.count;
        return (
            <Container>
                <View style={styles.headerView}>
                    <Search
                        ref="catgory_search"
                        placeholder="Categories"
                        afterFocus={() => {
                            console.log("After focus");
                        }}
                        onChangeText={text => {
                            this.filter('cat', text)
                        }}
                        onCancel={() => {
                            this.filter('cat', "")
                        }}
                        onDelete={() => {
                            this.filter('cat', "")
                        }}
                    />
                    <Search
                        ref="model_search"
                        placeholder="Make/Model"
                        afterFocus={() => {
                            console.log("After focus");
                        }}
                        onChangeText={text => {
                            this.filter('model', text)
                        }}
                        onCancel={() => {
                            this.filter('model', "")
                        }}
                        onDelete={() => {
                            this.filter('model', "")
                        }}
                    />
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center', marginVertical: 10 }}>
                    <Item style={{ width: 300 }}>
                        <Button small block info style={styles.navBtn}
                            onPress={() => this.goPageImediately(1)}
                            disabled={parseInt(this.state.currentPage) <= 1}
                        >
                            <Text style={{ textAlign: 'center' }}> {'<<'} </Text>
                        </Button>
                        <Button small block info style={styles.navBtn}
                            onPress={() => this.goPageImediately(parseInt(this.state.currentPage) - 1)}
                            disabled={parseInt(this.state.currentPage) <= 1}
                        >
                            <Text> {'<'} </Text>
                        </Button>
                        <Input keyboardType='numeric' style={{ fontSize: 15, paddingVertical: 0, flex: 1.5, textAlign: 'center', height: 32, backgroundColor: '#28B8F6', color: 'white' }} value={this.state.currentPage} onChangeText={(text) => {
                            let pageNum = parseInt(text) || 1;
                            pageNum = (pageNum > count) ? count : pageNum
                            pageNum = "" + pageNum
                            this.setState({
                                currentPage: pageNum
                            })
                            this.goPage()
                        }}></Input>
                        <Button small block info style={styles.navBtn}
                            onPress={() => this.goPageImediately(parseInt(this.state.currentPage) + 1)}
                            disabled={parseInt(this.state.currentPage) >= this.state.count}
                        >
                            <Text> {'>'} </Text>
                        </Button>
                        <Button small block info style={styles.navBtn}
                            onPress={() => this.goPageImediately(this.state.count)}
                            disabled={parseInt(this.state.currentPage) >= this.state.count}
                        >
                            <Text> {'>>'} </Text>
                        </Button>
                    </Item>
                </View>
                <Content style={{ backgroundColor: 'white' }}>
                    {this.state.isLoaded && !this.state.isRunning ? (
                        <List>
                            {
                                (this.state.models.length > 0) ?
                                    this.state.models.map((model) => {
                                        let categoryName = model.category ? model.category.name : 'n/a'
                                        let itemDiv = false;
                                        if (curDevider != categoryName) {
                                            curDevider = categoryName;
                                            itemDiv = true
                                        }
                                        return (
                                            <React.Fragment key={model.id}>
                                                {
                                                    itemDiv ? (
                                                        <ListItem noIndent itemDivider style={styles.itemDivider}>
                                                            <Text style={{ fontWeight: 'bold' }}>{curDevider}</Text>
                                                        </ListItem>
                                                    ) : null
                                                }
                                                <ListItem noIndent style={{ borderBottomColor: '#eee', borderBottomWidth: 1, paddingVertical: 3 }}>
                                                    <Body style={styles.body}>
                                                        <Row style={styles.row}>
                                                            <Col>
                                                                <Text>
                                                                    {model.name}
                                                                </Text>
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            <Col style={{ flex: 20 }}></Col>
                                                            <Col style={[styles.col1, { flex: 35 }]}>
                                                                <Picker
                                                                    mode="dropdown"
                                                                    iosIcon={<Icon name="ios-arrow-down-outline" />}
                                                                    placeholder="Select Status"
                                                                    placeholderStyle={{ color: "#bfc6ea" }}
                                                                    placeholderIconColor="#007aff"
                                                                    style={[styles.picker, { borderBottomWidth: 1 }]}
                                                                    selectedValue={model.status}
                                                                    onValueChange={(value) => this.onStatusChange(value, model)}
                                                                >
                                                                    {statusesPickerItems}
                                                                </Picker>
                                                            </Col>
                                                            <Col style={{ flex: 2 }}></Col>
                                                            <Col style={[styles.col2, { flex: 15 }]}>
                                                                <Badge info>
                                                                    <Text>
                                                                        {this.countPerStatus(model)}
                                                                    </Text>
                                                                </Badge>
                                                            </Col>
                                                            <Col style={{ flex: 8 }}></Col>
                                                            <Col style={[styles.col3, { flex: 25 }]}>
                                                                <Button full info style={{ height: 25 }} onPress={() => this.viewEquipments(model)}>
                                                                    <Text>View</Text>
                                                                </Button>
                                                            </Col>
                                                        </Row>
                                                    </Body>
                                                </ListItem >
                                            </React.Fragment>
                                        );
                                    })
                                    : null
                            }
                        </List>
                    ) : (
                            <Spinner color='#28B8F6' />
                        )
                    }
                </Content>
                <Fab
                    style={{ backgroundColor: '#5067FF' }}
                    position="bottomRight"
                    onPress={() => {
                        this.props.navigation.navigate('AddEquipment');
                    }}>
                    <Icon name="add" />
                </Fab>
            </Container>
        );
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        changeHeaderTitle: (headerTitle) => {
            dispatch(changeHeaderTitle(headerTitle))
        },
        visableSideBar: () => {
            dispatch(changeSideBarInivisble(false))
        }
    }
}

export default connect(undefined, mapDispatchToProps)(Home);
