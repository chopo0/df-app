import axios from './interceptor';
const modelsResource = '/api/models'

export default {
    index(data) {
        return axios.get(modelsResource, {
            params: data
        })
    },
    categorizedModels(id) {
        console.log("Categories : ", id);
        return axios.get(modelsResource + "?category_id=" + id)
    },
    pageNumberIndexed(pageno) {
        return axios.get(modelsResource + "?page=" + pageno)
    },
    category_based(category_id) {
        return axios.get(modelsResource + "?category_id=" + category_id);
    },
    store(data) {
        return axios.post(modelsResource, data)
    },
    patch(id, data) {
        return axios.patch(modelsResource + '/' + id, data)
    },
    show(id) {
        return axios.get(modelsResource + '/' + id)
    },
    delete(id) {
        return axios.delete(modelsResource + '/' + id)
    }
}