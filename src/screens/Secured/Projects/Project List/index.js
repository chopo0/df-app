import React from 'react';
import {
    connect
} from 'react-redux';
import { Container, Grid, Content, Item, List, ListItem, Body, Picker, Text, Button, Fab, Badge, Icon, Row, Col, Spinner, Input, Header, Thumbnail } from 'native-base';
import {
    ListView,
    Image,
    View,
    Alert
} from 'react-native';
import _ from 'lodash'
import EventEmitter from 'EventEmitter';
import moment from 'moment'

import NavigationService from '../../../../service/NavigationService'
import { fetchProjectSidebar } from '../../../../service/project_form'
import {
    changeHeaderTitle,
    changeSideBarInivisble,
    changeSideBarMenus
} from '../../../../actions/shared'
import {
    setProject,
    setProjectForms
} from '../../../../actions/project'
import {
    initPagination
} from '../../../../actions/pagination'
import {
    fetchUser
} from '../../../../service/fetch_user'

import {
    handleErrorResponse
} from '../../../../service/error_handler'
import {
    API_DOMAIN
} from '../../../../config/env'
import BaseComponent from '../../BaseComponent'
import PaginationComponent from '../../BaseComponent/pagination'
import styles from "./styles"
import apiProjects from '../../../../api/projects';
import apiProjectStatus from '../../../../api/project_status';
import apiTeams from '../../../../api/teams';
import LoadingComponent from '../../BaseComponent/loading'

class ProjectList extends BaseComponent {
    constructor(props) {
        super(props)

        this.state = {
            selectedYear: '' + new Date().getFullYear(),
            selectedStatus: null,
            selectedTeam: null,
            years: [],
            statuses: [],
            teams: [],
            filter: '',
            isBusy: false,
            perPage: 10,
            count: 0,
            projects: []
        }

        this.loadProjects = _.debounce(this.loadProjects.bind(this), 300);
        this.ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
    }

    componentDidMount() {
        this.subs = [
            this.props.navigation.addListener("didFocus", () => {
                this.props.changeHeaderTitle("Project List")
                this.props.changeSideBarMenus(['Project List', 'New Project'])
                this.props.visableSideBar()
                this.props.fetchUser()
                this.init()
                console.log("project list mounted")
            }),
            this.props.navigation.addListener("willBlur", () => { })
        ];
    }

    componentWillReceiveProps(props) {
        const pageNum = this.props.pageNum
        if (props.pageNum !== pageNum) {
            this.loadProjects()
        }
    }

    componentWillUnmount() {
        this.subs.forEach(sub => sub.remove());
    }

    init() {
        this.run()
        let years = []
        for (let year = new Date().getFullYear(); year >= 2015; year--) {
            years.push({
                value: '' + year,
                text: '' + year
            })
        }
        const apis = [
            apiProjects.index({
                per_page: this.state.perPage,
                year: Number(this.state.selectedYear)
            }),
            apiProjectStatus.index(),
            apiTeams.index()
        ]
        Promise.all(apis)
            .then(response => {
                let statuses = [{
                    text: '-- Filter by Status --',
                    value: null
                }]
                response[1].data.statuses.forEach(status => {
                    statuses.push({
                        text: status.name,
                        value: status.id
                    })
                })
                this.setState({
                    years: years,
                    projects: response[0].data.data || [],
                    count: Math.ceil(response[0].data.total / this.state.perPage),
                    teams: _.concat({ id: null, name: '-- Filter by Team --' }, response[2].data.data),
                    statuses: statuses
                })
                this.props.initPagination(Math.ceil(response[0].data.total / this.state.perPage), true)
                this.dataReady()
            }).catch(err => {
                this.dataFailed()
                handleErrorResponse(err)
            })
    }

    loadProjects(initPageNum = false) {
        this.run()
        let data = {
            page: Number(this.props.pageNum),
            filter: this.state.filter,
            per_page: this.state.perPage,
            status: this.state.selectedStatus,
            year: Number(this.state.selectedYear),
            assigned_to: this.state.selectedTeam
        }
        apiProjects.index(data)
            .then(response => {
                this.setState({
                    projects: response.data.data || [],
                    count: Math.ceil(response.data.total / this.state.perPage)
                })
                this.props.initPagination(Math.ceil(response.data.total / this.state.perPage), initPageNum)
                this.dataReady()
            }).catch(err => {
                this.dataFailed()
                handleErrorResponse(err)
            })
    }

    onConfigChange(value, attr) {
        let obj = {}
        obj[attr] = value
        this.setState(obj, () => {
            this.loadProjects(true)
        })
    }

    getImageUri(img) {
        if (img.indexOf('data:image/jpeg') !== -1 || img.indexOf('content://') !== -1) {
            return img;
        }
        let link = API_DOMAIN + '/' + img;
        return link
    }

    editProject(projectId) {
        this.props.fetchProjectSidebar(projectId)
        NavigationService.navigate('Call Report', {
            project_id: projectId,
            form_id: 1
        })
    }

    removeProject(projectId) {
        Alert.alert(
            'Delete Project Confirmation',
            'Are you sure?',
            [
                { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                {
                    text: 'Confirm', onPress: () => {
                        apiProjects.delete(projectId)
                            .then(() => {
                                this.loadProjects()
                            })
                            .catch(handleErrorResponse)
                    }
                },
            ],
            { cancelable: false }
        )
    }

    restoreProject(projectId) {
        apiProjects.restore({
            project_id: projectId
        }).then(response => {
            this.loadProjects(true)
        })
    }

    render() {
        let yearPickerItems = this.state.years && this.state.years.map((y, i) => {
            return <Picker.Item key={i} value={y.value} label={y.text} />
        })
        let statusPickerItems = this.state.statuses && this.state.statuses.map((s, i) => {
            return <Picker.Item key={i} value={s.value} label={s.text} />
        })
        let teamPickerItems = this.state.teams && this.state.teams.map((t, i) => {
            return <Picker.Item key={i} value={t.id} label={t.name} />
        })
        return (
            <Content padder>
                <View>
                    <Grid>
                        <Col>
                            <Picker
                                mode="dropdown"
                                iosIcon={<Icon name="ios-arrow-down-outline" />}
                                placeholder="Select Year"
                                placeholderStyle={{ color: "#bfc6ea" }}
                                style={[]}
                                placeholderIconColor="#007aff"
                                selectedValue={this.state.selectedYear}
                                onValueChange={(value) => this.onConfigChange(value, 'selectedYear')}
                            >
                                {yearPickerItems}
                            </Picker>

                            <Picker
                                mode="dropdown"
                                iosIcon={<Icon name="ios-arrow-down-outline" />}
                                placeholder="Select Status"
                                placeholderStyle={{ color: "#bfc6ea" }}
                                placeholderIconColor="#007aff"
                                style={[]}
                                selectedValue={this.state.selectedStatus}
                                onValueChange={(value) => this.onConfigChange(value, 'selectedStatus')}
                            >
                                {statusPickerItems}
                            </Picker>
                        </Col>
                        <Col style={{ paddingHorizontal: 15 }}>
                            {this.props.company.logo ? (
                                <Image resizeMode="contain" style={{ flex: 1 }} source={{ uri: this.getImageUri(this.props.company.public_logo_path) }} />
                            ) : (
                                    <Image resizeMode="contain" style={{ flex: 1 }} source={require('../../../../../assets/fallback-logo.jpg')} />
                                )}
                        </Col>
                        <Col>
                            <Item>
                                <Icon name="ios-search" />
                                <Input placeholder="Search" value={this.state.filter} onChangeText={(text) => this.onConfigChange(text, 'filter')} />
                            </Item>
                            {this.props.user.role && this.props.user.role.name !== 'user' ? (
                                <Picker
                                    mode="dropdown"
                                    iosIcon={<Icon name="ios-arrow-down-outline" />}
                                    placeholder="Select Team"
                                    placeholderStyle={{ color: "#bfc6ea" }}
                                    placeholderIconColor="#007aff"
                                    style={[]}
                                    selectedValue={this.state.selectedTeam}
                                    onValueChange={(value) => this.onConfigChange(value, 'selectedTeam')}
                                >
                                    {teamPickerItems}
                                </Picker>
                            ) : null}
                        </Col>
                    </Grid>
                </View>
                <PaginationComponent />
                <View style={[styles.listView, { marginTop: 10 }]}>
                    <Text style={[styles.defaultHeaderText, { flex: 1 }]} numberOfLines={1}>
                        Owner/Insured
                    </Text>
                    <Text style={[styles.defaultHeaderText, { flex: 1 }]} numberOfLines={1}>
                        Address
                    </Text>
                    <Text style={[styles.defaultHeaderText, { flex: 1 }]} numberOfLines={1}>
                        Phone
                    </Text>
                    <Text style={[styles.defaultHeaderText, { flex: 1 }]} numberOfLines={1}>
                        Assigned To
                    </Text>
                    <Text style={[styles.defaultHeaderText, { flex: 1 }]} numberOfLines={1}>
                        Status
                    </Text>
                    <Text style={[styles.defaultHeaderText, { flex: 1 }]} numberOfLines={1}>
                        Created
                    </Text>
                </View>
                {this.state.isRunning ? (
                    <LoadingComponent />
                ) : (
                        <List style={{}}
                            dataSource={this.ds.cloneWithRows(this.state.projects)}
                            renderRow={data =>
                                <View style={styles.listView}>
                                    <View style={{ flex: 1 }}>
                                        <Text style={[styles.text]} numberOfLines={1}>
                                            {data.owner_name ? data.owner_name : 'n/a'}
                                        </Text>
                                    </View>
                                    <View style={{ flex: 1 }}>
                                        <Text style={[styles.defaultText]} numberOfLines={1}>
                                            {data.project_call_report && data.project_call_report.short_job_address ? data.project_call_report.short_job_address : 'n/a'}
                                        </Text>
                                    </View>
                                    <View style={{ flex: 1 }}>
                                        <Text style={[styles.text]} numberOfLines={1}>
                                            {data.phone ? data.phone : 'n/a'}
                                        </Text>
                                    </View>
                                    <View style={{ flex: 1 }}>
                                        <Text style={[styles.defaultText]} numberOfLines={1}>
                                            {data.assignee ? data.assignee.name : 'n/a'}
                                        </Text>
                                    </View>
                                    <View style={{ flex: 1 }}>
                                        <Text style={[styles.text]} numberOfLines={1}>
                                            {data.status_info ? data.status_info.name : 'n/a'}
                                        </Text>
                                    </View>
                                    <View style={{ flex: 1 }}>
                                        <Text style={[styles.defaultText]} numberOfLines={1}>
                                            {moment(data.project_call_report.date_contacted).format('MM/DD/YYYY')}
                                        </Text>
                                    </View>
                                </View>}
                            renderLeftHiddenRow={data => {
                                return (
                                    <React.Fragment>
                                        {
                                            (data.status !== 3) ? (
                                                <Button full info onPress={() => { this.editProject(data.id) }}>
                                                    <Icon active name="md-create" />
                                                </Button>) : (
                                                    <Button full info onPress={() => { this.restoreProject(data.id) }}>
                                                        <Icon active name="ios-refresh" />
                                                    </Button>
                                                )
                                        }
                                    </React.Fragment>
                                )
                            }}
                            renderRightHiddenRow={(data) => {
                                return (
                                    <React.Fragment>
                                        {
                                            (data.status !== 3) ? (
                                                <Button full danger onPress={() => { this.removeProject(data.id) }}>
                                                    <Icon active name="trash" />
                                                </Button>) : null
                                        }
                                    </React.Fragment>
                                )
                            }}
                            leftOpenValue={50}
                            rightOpenValue={-50}
                        />
                    )
                }

            </Content>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        pageNum: state.pagination.pageNum,
        pageCount: state.pagination.pageCount,
        user: state.user.user,
        company: state.user.company
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        changeHeaderTitle: (headerTitle) => {
            dispatch(changeHeaderTitle(headerTitle))
        },
        visableSideBar: () => {
            dispatch(changeSideBarInivisble(false))
        },
        fetchUser: () => {
            dispatch(fetchUser())
        },
        initPagination: (pageCount, isPageNumInit = false) => {
            dispatch(initPagination(pageCount, isPageNumInit))
        },
        changeSideBarMenus: (sideBarMenus) => {
            dispatch(changeSideBarMenus(sideBarMenus))
        },
        setProject: (projectId, callReport) => {
            dispatch(setProject(projectId, callReport))
        },
        fetchProjectSidebar: (projectId) => {
            dispatch(fetchProjectSidebar(projectId))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProjectList);