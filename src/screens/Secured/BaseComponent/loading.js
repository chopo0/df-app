import React, {
    Component
} from 'react';
import { Spinner } from 'native-base';
class LoadingComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }

    render() {
        return (
            <Spinner color='#28B8F6' />
        )
    }
}

export default LoadingComponent;