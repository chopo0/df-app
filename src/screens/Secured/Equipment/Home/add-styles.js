var React = require("react-native");
var {
    Dimensions
} = React;

import commonColor from "../../../../theme/variables/commonColor";

export default {
    col1: {
        // backgroundColor: 'blue'
    },
    col2: {
        // backgroundColor:'red',
        justifyContent: 'center',
        height: 25,
    },
    col3: {
        height: 25,
    },
    colItem1: {
        borderWidth: 0,
    },
    colItem2: {
        borderWidth: 0,
    },
    itemDivider: {
        backgroundColor: "#a8d9d5",
        paddingVertical: 0,
        height: 30
    },
    picker: {
        height: 25,
        transform: [{
                scaleX: 0.9
            },
            {
                scaleY: 0.9
            },
        ]
    },
    picker1: {
        height: 32,
        backgroundColor: '#a8d9d5'
    },
    picker2: {
        height: 32,
        backgroundColor: '#a8d9d5',
        // color: 'white'
    },
    location: {
        height: 32,
        // borderColor: '#b85dd2',
        // color: 'white'
    },
    edit: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 44
    },
    quantity: {
        flex: 4,
        marginRight: 40,
        paddingHorizontal: 30,
        justifyContent: 'center',
        alignItems: 'center'
    },
    navBtn: {
        margin: 0,
        flex: 1,
        justifyContent: 'center'
    },
    validate: {
        height: 32,
        justifyContent: 'center',
        alignItems: 'center',
        width: 44
    }
};