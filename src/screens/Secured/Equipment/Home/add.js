import React, { Component } from 'react';
import { Modal, Switch, TouchableOpacity, Alert } from 'react-native';
import { connect } from 'react-redux';
import { Container, Content, Form, View, Icon, Picker, Input, Label, Item, Button, Text, CheckBox, Body, Toast } from 'native-base';
import shallowCompare from 'react-addons-shallow-compare';
import { changeHeaderTitle, changeSideBarInivisble } from '../../../../actions/shared';
import {
    fetchUser
} from '../../../../service/fetch_user'
import EquipmentNumbers from './modal/equipment-numbers'
import NavigationService from '../../../../service/NavigationService';
import apiStatus from '../../../../api/status';
import apiTeams from '../../../../api/teams';
import apiCategories from '../../../../api/categories';
import apiEquipments from '../../../../api/equipment';
import apiModels from '../../../../api/models';
import BaseComponent from '../../BaseComponent';
import {
    handleErrorResponse
} from '../../../../service/error_handler'
import LoadingComponent from '../../BaseComponent/loading'
import styles from './add-styles'

class AddEquipment extends BaseComponent {
    constructor(props) {
        super(props);
        this.state = {
            teams: [{
                id: null,
                name: "--Select--"
            }],
            statuses: [{
                id: null,
                name: "--Select--"
            }],
            categories: [{
                id: null,
                name: "--Select--"
            }],
            models: [{
                id: null,
                name: "--Select--"
            }],
            companyId: '',
            newCategory: "",
            newPrefix: "",
            newModel: "",
            automatic: true,
            visibleModal: false,
            equipment: {
                category_id: null,
                model_id: null,
                team_id: null,
                status_id: 1,
                quantity: "1",
                serials: []
            }
        }
        this.initData = this.initData.bind(this);
        this.changedCategory = this.changedCategory.bind(this);
        this.dismissModal = this.dismissModal.bind(this);
        this.addEquip = this.addEquip.bind(this);
        this.saveEquipments = this.saveEquipments.bind(this)
    }

    componentDidMount() {
        this.subs = [
            this.props.navigation.addListener("didFocus", () => {
                this.props.fetchUser()
                this.props.invisableSideBar();
                this.props.changeHeaderTitle("Add New Equipment");
                this.initData();
                console.log("Equipments add mounted");
            }),
            this.props.navigation.addListener("willBlur", () => {
            })
        ]
    }

    componentWillUnmount() {
        this.subs.forEach(sub => sub.remove());
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (shallowCompare(this, nextProps, nextState)) {
            if (nextProps.company.id && !this.state.companyId) {
                nextState.companyId = nextProps.company.id
            }
            return true
        } else {
            return false;
        }
    }

    initData() {
        this.setState({
            isLoaded: false
        }, () => {
            Promise.all([
                apiCategories.index(),
                apiStatus.index(),
                apiTeams.index(),
            ])
                .then(result => {
                    const categories = result[0].data.data;
                    const statuses = result[1].data.data;
                    const teams = result[2].data.data;
                    this.setState({
                        categories: [...this.state.categories, ...categories],
                        statuses: [...this.state.statuses, ...statuses],
                        teams: [...this.state.teams, ...teams]
                    });
                    this.dataReady()
                    this.loaded()
                })
                .catch(err => {
                    handleErrorResponse(err)
                })
        })
    };

    changedCategory() {
        if (!this.state.equipment.category_id) {
            this.setState((preState) => ({
                equipment: Object.assign({}, preState.equipment, {
                    model_id: null
                }),
                models: [{
                    id: null,
                    name: "--Select--"
                }]
            }))
        } else {
            this.setState((preState) => ({
                equipment: Object.assign({}, preState.equipment, {
                    model_id: null
                }),
                models: [{
                    id: null,
                    name: "--Select--"
                }]
            }), () => {
                apiModels.index({ category_id: this.state.equipment.category_id, show_all: true })
                    .then(response => {
                        this.setState({
                            models: [...this.state.models, ...response.data.data]
                        })
                    })
            })
        }
    }

    showModal() {
        if (!this.state.equipment.category_id && (!this.state.newCategory || !this.state.newPrefix)) {
            Toast.show({
                text: "Please input valid category",
                duration: 3000,
                type: "error"
            })
            return;
        }
        this.setState({
            automatic: false,
            visibleModal: true
        })
    }

    dismissModal() {
        this.setState({
            visibleModal: false
        });
    }

    addEquip(serials) {
        this.setState((preState) => ({
            equipment: Object.assign({}, preState.equipment, {
                serials: serials
            })
        }))
    }

    editEquip() {
        if (this.state.automatic) return;
        this.setState({
            visibleModal: true
        });
    }

    clearEquip() {
        if (this.state.automatic) return;
        this.setState((preState) => ({
            equipment: Object.assign({}, preState.equipment, {
                serials: []
            })
        }));
    }

    validate() {
        if (!this.state.equipment.category_id || !this.state.equipment.model_id) {
            if (this.state.equipment.category_id) {
                if (!this.state.newModel) return false
            } else if (this.state.equipment.model_id) {
                if (!this.state.newCategory || !this.state.newModel) return false
            } else {
                if (!this.state.newCategory || !this.state.newPrefix || !this.state.newModel) return false
            }
        }
        if (!this.state.equipment.quantity || !this.state.equipment.status_id) {
            return false
        }
        return true
    }

    saveEquipments() {
        if (!this.state.automatic) {
            let complete = true
            for (let index = 0; index < this.state.equipment.quantity; index++) {
                const serial = this.state.equipment.serials[index];
                if (!serial || !serial.validate) {
                    complete = false
                    break;
                }
            }
            if (!complete) {
                Toast.show({
                    text: "Please enter valid serial numbers",
                    duration: 3000,
                    type: "error"
                })
                return
            }
        }
        if (!this.validate()) {
            Toast.show({
                text: "Please check all fields[Category, Make/Model, Status, Quantity]",
                duration: 3000,
                type: "error"
            })
            return
        }
        var saveEquipment = () => {
            let payload = Object.assign({}, this.state.equipment)
            payload.company_id = this.state.companyId
            payload.quantity = Number(payload.quantity)
            if (this.state.automatic) {
                payload.auto_assign = 'yes'
                delete payload.serials
            } else {
                payload.auto_assign = 'no'
            }
            return apiEquipments.store(payload)
                .then(response => {
                    if (response.data.message === 'error') {
                        let validate = response.data.validate.serials || {
                            exists: [],
                            nonexistences: []
                        }
                        if (validate.exists.length !== 0) {
                            Toast.show({
                                text: "Some serial numbers already exist",
                                duration: 3000,
                                type: "error"
                            })
                        }
                    } else {
                        Toast.show({
                            text: "Serial Numbers successfully added",
                            duration: 3000,
                            type: "success"
                        })
                        this.setState({
                            newCategory: "",
                            newPrefix: "",
                            newModel: "",
                            automatic: true,
                            visibleModal: false,
                            equipment: {
                                category_id: null,
                                model_id: null,
                                team_id: null,
                                status_id: 1,
                                quantity: "1",
                                serials: []
                            }
                        })
                    }
                }).catch(handleErrorResponse)
        }
        var saveModel = () => {
            return apiModels.store({
                name: this.state.newModel,
                category_id: this.state.equipment.category_id,
                company_id: this.state.companyId
            }).then(response => {
                if (response.data.message) {
                    this.setState((preState) => ({
                        equipment: Object.assign({}, preState.equipment, {
                            model_id: response.data.model.id
                        }),
                        models: [...this.state.models, ...[{
                            id: response.data.model.id,
                            name: response.data.model.name
                        }]]
                    }))
                    return saveEquipment()
                }
            }).catch(handleErrorResponse)
        }
        if (!this.state.equipment.category_id) {
            return apiCategories.store({
                name: this.state.newCategory,
                prefix: this.state.newPrefix,
                company_id: this.state.companyId
            }).then(response => {
                this.setState((preState) => ({
                    equipment: Object.assign({}, preState.equipment, {
                        category_id: response.data.category.id
                    }),
                    categories: [...this.state.categories, ...[{
                        id: response.data.category.id,
                        name: response.data.category.name
                    }]]
                }))
                if (!this.state.equipment.model_id) {
                    return saveModel()
                }
                return saveEquipment()
            }).catch(handleErrorResponse)
        }

        if (!this.state.equipment.model_id) {
            return saveModel()
        }
        return saveEquipment()
    }

    render() {
        let categoriesPickerItems = this.state.categories && this.state.categories.map((c, i) => {
            return <Picker.Item key={i} value={c.id} label={c.name} />
        })
        let modelsPickerItems = this.state.models && this.state.models.map((m, i) => {
            return <Picker.Item key={i} value={m.id} label={m.name} />
        })
        let teamsPickerItems = this.state.teams && this.state.teams.map((t, i) => {
            return <Picker.Item key={i} value={t.id} label={t.name} />
        })
        let statusesPickerItems = this.state.statuses && this.state.statuses.map((s, i) => {
            return <Picker.Item key={i} value={s.id} label={s.name} />
        })
        let complete = true

        for (let index = 0; index < this.state.equipment.quantity; index++) {
            const serial = this.state.equipment.serials[index];
            if (!serial || !serial.validate) {
                complete = false
                break;
            }
        }

        return (
            <Content padder>
                {
                    this.isLoaded() && this.state.companyId ? (
                        <Form>
                            <Item>
                                <Label style={{ fontWeight: 'bold' }}>Category:</Label>
                                <Picker
                                    mode="dropdown"
                                    iosIcon={<Icon name="ios-arrow-down-outline" />}
                                    placeHolder="Select Category"
                                    placeholderStyle={{ color: "#bfc6ea" }}
                                    placeholderIconColor="#007aff"
                                    selectedValue={this.state.equipment.category_id}
                                    onValueChange={(value) => {
                                        this.setState((preState) => ({
                                            equipment: Object.assign({}, preState.equipment, {
                                                category_id: value
                                            })
                                        }), () => {
                                            this.changedCategory()
                                        })
                                    }}
                                >
                                    {categoriesPickerItems}
                                </Picker>
                            </Item>
                            {!this.state.equipment.category_id ? (
                                <Item>
                                    <Label style={{ fontWeight: 'bold' }}>Add New Cateogry:</Label>
                                    <Input placeholder="Category" value={this.state.newCategory} onChangeText={(value) => {
                                        this.setState({
                                            newCategory: value
                                        })
                                    }} />
                                    <Input placeholder="Prefix" value={this.state.newPrefix} onChangeText={(value) => {
                                        this.setState({
                                            newPrefix: value
                                        })
                                    }} />
                                </Item>
                            ) : null}

                            <Item>
                                <Label style={{ fontWeight: 'bold' }}>Make/Model:</Label>
                                <Picker
                                    mode="dropdown"
                                    iosIcon={<Icon name="ios-arrow-down-outline" />}
                                    placeHolder="Select Make/Model"
                                    placeholderStyle={{ color: "#bfc6ea" }}
                                    placeholderIconColor="#007aff"
                                    selectedValue={this.state.equipment.model_id}
                                    onValueChange={(value) => {
                                        this.setState((preState) => ({
                                            equipment: Object.assign({}, preState.equipment, {
                                                model_id: value
                                            })
                                        }))
                                    }}
                                >
                                    {modelsPickerItems}
                                </Picker>
                            </Item>
                            {!this.state.equipment.model_id ? (
                                <Item inlineLabel>
                                    <Label style={{ fontWeight: 'bold' }}>Add New Make/Model:</Label>
                                    <Input placeholder="Make/Model :" value={this.state.newModel} onChangeText={(value) => {
                                        this.setState({
                                            newModel: value
                                        })
                                    }} />
                                </Item>
                            ) : null}

                            <Item>
                                <Label style={{ fontWeight: 'bold' }}>Crew/Team:</Label>
                                <Picker
                                    mode="dropdown"
                                    iosIcon={<Icon name="ios-arrow-down-outline" />}
                                    placeHolder="Select Crew/Team"
                                    placeholderStyle={{ color: "#bfc6ea" }}
                                    placeholderIconColor="#007aff"
                                    selectedValue={this.state.equipment.team_id}
                                    onValueChange={(value) => {
                                        this.setState((preState) => ({
                                            equipment: Object.assign({}, preState.equipment, {
                                                team_id: value
                                            })
                                        }))
                                    }}
                                >
                                    {teamsPickerItems}
                                </Picker>
                            </Item>

                            <Item>
                                <Label style={{ fontWeight: 'bold' }}>Statuses:</Label>
                                <Picker
                                    mode="dropdown"
                                    iosIcon={<Icon name="ios-arrow-down-outline" />}
                                    placeHolder="Select Status"
                                    placeholderStyle={{ color: "#bfc6ea" }}
                                    placeholderIconColor="#007aff"
                                    selectedValue={this.state.equipment.status_id}
                                    onValueChange={(value) => {
                                        this.setState((preState) => ({
                                            equipment: Object.assign({}, preState.equipment, {
                                                status_id: value
                                            })
                                        }))
                                    }}
                                >
                                    {statusesPickerItems}
                                </Picker>
                            </Item>
                            <Item inlineLabel>
                                <Label style={{ fontWeight: 'bold' }}>Quantity:</Label>
                                <Input keyboardType='numeric' style={[]} value={this.state.equipment.quantity} onChangeText={(value) => {
                                    this.setState((preState) => ({
                                        equipment: Object.assign({}, preState.equipment, {
                                            quantity: value
                                        })
                                    }))
                                }} />
                            </Item>
                            <Item style={{}}>
                                <View style={{
                                    flexDirection: "column",
                                    justifyContent: "space-between",
                                    alignContent: "center",
                                    alignItems: 'center',
                                    flex: 10
                                }}>
                                    <Switch
                                        value={this.state.automatic}
                                        onValueChange={(value) => {
                                            if (!value) {
                                                this.showModal()
                                            } else {
                                                this.setState({
                                                    automatic: true,
                                                    visibleModal: false
                                                });
                                            }
                                        }}
                                    />
                                    <Text style={{
                                        fontSize: 11,
                                    }}>Assign Equipment Numbers Automatically?</Text>
                                </View>
                                {this.state.automatic ? (
                                    null
                                ) : (
                                        <View style={{ flex: 4, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                            {!complete ? (
                                                <TouchableOpacity onPress={() => { }} style={[styles.edit, { backgroundColor: '#fa8072' }]} ><Text><Icon name="md-alert" style={{ color: 'white' }}></Icon></Text></TouchableOpacity>
                                            ) : null}
                                            <TouchableOpacity style={[styles.edit, { backgroundColor: '#28B8F6' }]} onPress={() => { this.editEquip() }}><Text><Icon name="md-create" style={{ color: 'white' }}></Icon></Text></TouchableOpacity>
                                            <TouchableOpacity style={[styles.edit, { backgroundColor: '#fa8072' }]} onPress={() => { this.clearEquip() }}><Text><Icon name="md-refresh" style={{ color: 'white' }}></Icon></Text></TouchableOpacity>
                                        </View>
                                    )}
                            </Item>
                            <Button block info style={{ margin: 15, marginTop: 30 }} onPress={() => { this.saveEquipments() }}>
                                <Text> Submit </Text>
                            </Button>
                            <Modal
                                visible={this.state.visibleModal}
                                transparent={false}
                                animationType={"slide"}
                                onRequestClose={this.dismissModal}>
                                <EquipmentNumbers
                                    category_id={this.state.equipment.category_id}
                                    newCategory={this.state.newCategory}
                                    quantity={this.state.equipment.quantity}
                                    dismissModal={this.dismissModal}
                                    addEquip={this.addEquip}
                                    serials={this.state.equipment.serials}
                                ></EquipmentNumbers>
                            </Modal>
                        </Form >
                    ) : (
                            <LoadingComponent />
                        )
                }
            </Content>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user.user,
        company: state.user.company
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        changeHeaderTitle: (headerTitle) => {
            dispatch(changeHeaderTitle(headerTitle))
        },
        invisableSideBar: () => {
            dispatch(changeSideBarInivisble(true))
        },
        fetchUser: () => {
            dispatch(fetchUser())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddEquipment);
