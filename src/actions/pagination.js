import {
    GO_PAGE,
    INIT_PAGINATION
} from './actionTypes'

export const goPage = (pageNum, pageCount) => {
    return {
        type: GO_PAGE,
        pageNum: pageNum,
        pageCount: pageCount
    }
}

export const initPagination = (pageCount, isPageNumInit) => {
    return {
        type: INIT_PAGINATION,
        pageCount: pageCount,
        isPageNumInit: isPageNumInit
    }
}