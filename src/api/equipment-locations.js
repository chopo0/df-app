import axios from './interceptor';

const equipmentLocationsResource = '/api/equipment/locations'

export default {
    index (data) {
        return axios.get(equipmentLocationsResource, {params: data})
    },
    store (data) {
        return axios.post(equipmentLocationsResource, data)
    },
    patch (id, data) {
        return axios.patch(equipmentLocationsResource + '/' + id, data)
    }
}
