import React from 'react';
import {
    connect
} from 'react-redux';
import { Container, Grid, Content, Item, List, ListItem, Body, Picker, Text, Button, Fab, Badge, Icon, Row, Col, Spinner, Input, Header, Thumbnail, CheckBox, Toast, Label } from 'native-base';
import {
    ListView,
    FlatList,
    View
} from 'react-native';
import _ from 'lodash'
import { Alert } from 'react-native'
import EventEmitter from 'EventEmitter';
import shallowCompare from 'react-addons-shallow-compare';
import moment from 'moment'
import DatePicker from 'react-native-datepicker'

import NavigationService from '../../../../../service/NavigationService'
import {
    changeHeaderTitle,
    changeSideBarInivisble
} from '../../../../../actions/shared'
import {
    setCompanyDetails
} from '../../../../../actions/user'
import {
    handleErrorResponse
} from '../../../../../service/error_handler'
import BaseComponent from '../../../BaseComponent'
import styles from "./styles"
import apiFormsOrder from '../../../../../api/forms_order'
import { fetchFormsOrder } from '../../../../../service/standard_form'
import {
    fetchUser
} from '../../../../../service/fetch_user'
import LoadingComponent from '../../../BaseComponent/loading'
import FormHeader from '../Base/FormHeader'
import apiProjects from '../../../../../api/projects';
import apiProjectCallReports from '../../../../../api/project_call_reports';
import apiProjectForms from '../../../../../api/project_forms';
import apiTeams from '../../../../../api/teams'

StatesJson = require('../../../../../config/states.json')
class FormCallReport extends BaseComponent {
    constructor(props) {
        super(props)

        this.state = {
            projectId: null,
            formId: null,
            callReport: {
                id: null,
                company_id: null,
                project_id: null,
                contact_name: null,
                contact_phone: null,
                site_phone: null,
                date_contacted: null,
                time_contacted: null,
                date_loss: null,
                point_loss: null,
                date_completed: null,
                category: null,
                class: null,
                job_address: null,
                city: null,
                state: null,
                zip_code: null,
                cross_streets: null,
                apartment_name: null,
                building_no: null,
                apartment_no: null,
                gate_code: null,
                assigned_to: null,
                is_residential: null,
                is_commercial: null,
                is_insured: null,
                is_tenant: null,
                is_water: null,
                is_sewage: null,
                is_mold: null,
                is_fire: null,
                insured_name: null,
                billing_address: null,
                insured_city: null,
                insured_state: null,
                insured_zip_code: null,
                insured_home_phone: null,
                insured_cell_phone: null,
                insured_work_phone: null,
                insured_email: null,
                insured_fax: null,
                insurance_claim_no: null,
                insurance_company: null,
                insurance_policy_no: null,
                insurance_deductible: null,
                insurance_adjuster: null,
                insurance_address: null,
                insurance_city: null,
                insurance_state: null,
                insurance_zip_code: null,
                insurance_work_phone: null,
                insurance_cell_phone: null,
                insurance_email: null,
                insurance_fax: null
            },
            assignee: null,
            project: null,
            teams: [],
            isLoaded: false,
            states: Object.keys(StatesJson).map(function (key) {
                return {
                    value: key,
                    text: StatesJson[key]
                }
            }),
            customerTypes: [
                { text: 'Residential', value: 'is_residential' },
                { text: 'Commercial', value: 'is_commercial' },
                { text: 'Owner/Insured', value: 'is_insured' },
                { text: 'Tenant', value: 'is_tenant' }
            ],
            customerTypesMatch: {
                isResidential: 'is_residential',
                isCommercial: 'is_commercial',
                isInsured: 'is_insured',
                isTenant: 'is_tenant'
            },
            selectedCustomerType: [],
            sameJobAddress: false,
            sameContactName: false,
            timeConfig: {
                format: 'hh:mm:ss',
                useCurrent: false,
                showClear: true,
                showClose: true,
                keepOpen: true,
                debug: true
            },
            dateConfig: {
                format: 'YYYY/MM/D ',
                useCurrent: false,
                showClear: true,
                showClose: true,
                keepOpen: true
            }
        }

        this.save = _.debounce(this.save.bind(this), 500);
    }

    componentDidMount() {
        this.subs = [
            this.props.navigation.addListener("didFocus", () => {
                this.props.changeHeaderTitle("Call Report")
                this.props.visableSideBar()
                let params = this.props.navigation.state.params
                this.setState({
                    projectId: params.project_id,
                    formId: params.form_id
                }, () => {
                    this.init()
                })
                console.log("call report mounted")
            }),
            this.props.navigation.addListener("willBlur", () => { })
        ];
    }

    componentWillUnmount() {
        this.subs.forEach(sub => sub.remove());
    }

    init() {
        this.run()
        const apis = [
            apiProjectCallReports.index(this.state.projectId, {}),
            apiTeams.index()
        ]
        Promise.all(apis).then(response => {
            this.props.setCompanyDetails(response[0].data.project.company_details)
            this.setState({
                teams: response[1].data.data,
                callReport: response[0].data.call_report,
            }, () => {
                this.dataReady()
            })
        })
    }

    changeSelectState(attr, isRoot = false) {
        if (!isRoot) {
            this.setState((preState) => ({
                callReport: Object.assign({}, preState.callReport, {
                    [attr]: '' + (1 - Number(this.state.callReport[attr]))
                })
            }), () => {
                this.save()
            })
        } else {
            let obj = {}
            obj[attr] = !this.state[attr]
            this.setState(obj, () => {
                if (attr == 'sameContactName') {
                    this.setState((preState) => ({
                        callReport: Object.assign({}, preState.callReport, {
                            insured_name: this.state.sameContactName ? this.state.callReport.contact_name : null
                        })
                    }), () => {
                        this.save()
                    })
                } else if (attr == 'sameJobAddress') {
                    this.setState((preState) => ({
                        callReport: Object.assign({}, preState.callReport, {
                            billing_address: this.state.sameJobAddress ? this.state.callReport.job_address : null,
                            insured_city: this.state.sameJobAddress ? this.state.callReport.city : null,
                            insured_state: this.state.sameJobAddress ? this.state.callReport.state : null,
                            insured_zip_code: this.state.sameJobAddress ? this.state.callReport.zip_code : null,
                        })
                    }), () => {
                        this.save()
                    })
                }
            })
        }
    }

    changeInputState(attr, val) {
        this.setState((preState) => ({
            callReport: Object.assign({}, preState.callReport, {
                [attr]: val
            })
        }), () => {
            this.save()
        })
    }

    changeDateState(attr, val) {
        this.setState((preState) => ({
            callReport: Object.assign({}, preState.callReport, {
                [attr]: val
            })
        }), () => {
            this.save()
        })
    }

    save() {
        return apiProjectCallReports.update(this.state.callReport.id, this.state.callReport).then(response => {
            this.setState({
                callReport: response.data.call_report
            })
            if (response.data.date_completed_added) {
                this.sendReviewRequest(this.state.projectId)
            }
            if (response.data.has_set_equipment) {
                this.unsetEquipment(this.state.projectId)
            }
        }).catch(response => {
            handleErrorResponse(response)
        })
    }

    sendReviewRequest(id) {
        Alert.alert(
            'Review Request',
            'Would you like to send review request?',
            [
                { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                {
                    text: 'Confirm', onPress: () => {
                        apiProjects.requestReview(id)
                            .then(response => {
                                Toast.show({
                                    text: response.data.message,
                                    duration: 3000,
                                    type: "success"
                                });
                            })
                            .catch(error => {
                                handleErrorResponse(error)
                            })
                    }
                },
            ],
            { cancelable: false }
        )
    }

    unsetEquipment(id) {
        Alert.alert(
            'Pick Up Equipment',
            'You still have equipment set! Please pick up your equipment before this project can be completed.',
            [
                { text: 'Ok', onPress: () => console.log('Ok Pressed'), style: 'cancel' }
            ],
            { cancelable: false }
        )
    }

    render() {
        let statesPickerItems = this.state.states && this.state.states.map((s, i) => {
            return <Picker.Item key={i} value={s.value} label={s.text} />
        })
        let teamsPickerItems = this.state.teams && this.state.teams.map((t, i) => {
            return <Picker.Item key={i} value={t.id} label={t.name} />
        })
        return (
            <Container>
                {!this.state.isRunning ? (
                    <Content padder>
                        <FormHeader />
                        <Grid style={{ borderTopColor: 'grey', borderTopWidth: 2, paddingTop: 15 }}>
                            <Col>
                                <Text style={[styles.defaultText, styles.sectionHeaderText]}>Job Site</Text>
                                <Row>
                                    <Col>
                                        <ListItem style={styles.listView}>
                                            <CheckBox checked={this.state.callReport.is_residential == '1'} onPress={() => { this.changeSelectState('is_residential') }} />
                                            <Text style={[styles.defaultText]}>
                                                Residential
                                            </Text>
                                        </ListItem>
                                    </Col>
                                    <Col>
                                        <ListItem style={styles.listView}>
                                            <CheckBox checked={this.state.callReport.is_commercial == '1'} onPress={() => { this.changeSelectState('is_commercial') }} />
                                            <Text style={[styles.defaultText]}>
                                                Commercial
                                            </Text>
                                        </ListItem>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <ListItem style={styles.listView}>
                                            <CheckBox checked={this.state.callReport.is_insured == '1'} onPress={() => { this.changeSelectState('is_insured') }} />
                                            <Text style={[styles.defaultText]}>
                                                Owner/Insured
                                            </Text>
                                        </ListItem>
                                    </Col>
                                    <Col>
                                        <ListItem style={styles.listView}>
                                            <CheckBox checked={this.state.callReport.is_tenant == '1'} onPress={() => { this.changeSelectState('is_tenant') }} />
                                            <Text style={[styles.defaultText]}>
                                                Tenant
                                            </Text>
                                        </ListItem>
                                    </Col>
                                </Row>
                                <Item>
                                    <Label>Contact Name:</Label>
                                    <Input onChangeText={(text) => this.changeInputState("contact_name", text)} value={this.state.callReport.contact_name} />
                                </Item>
                                <Item>
                                    <Label>Contact Phone:</Label>
                                    <Input onChangeText={(text) => this.changeInputState("contact_phone", text)} value={this.state.callReport.contact_phone} placeholder="(702) 555-1212" />
                                </Item>
                                <Item>
                                    <Label>Site Phone:</Label>
                                    <Input onChangeText={(text) => this.changeInputState("site_phone", text)} value={this.state.callReport.site_phone} placeholder="(702) 555-1212" />
                                </Item>
                                <Item>
                                    <Label>Date Contacted:</Label>
                                    <DatePicker
                                        customStyles={{
                                            dateIcon: {
                                                display: 'none'
                                            },
                                            dateInput: {
                                                borderWidth: 0
                                            }
                                        }}
                                        mode="date"
                                        date={this.state.callReport.date_contacted}
                                        placeholder="select date"
                                        format="MM/DD/YYYY"
                                        onDateChange={(date) => this.changeDateState("date_contacted", date)}
                                    />
                                    <Button transparent onPress={() => this.changeDateState("date_contacted", null)}>
                                        <Icon name='trash'></Icon>
                                    </Button>
                                </Item>
                                <Item>
                                    <Label>Date of Loss:</Label>
                                    <DatePicker
                                        customStyles={{
                                            dateIcon: {
                                                display: 'none'
                                            },
                                            dateInput: {
                                                borderWidth: 0
                                            }
                                        }}
                                        mode="date"
                                        date={this.state.callReport.date_loss}
                                        placeholder="select date"
                                        format="MM/DD/YYYY"
                                        onDateChange={(date) => this.changeDateState("date_loss", date)}
                                    />
                                    <Button transparent onPress={() => this.changeDateState("date_loss", null)}>
                                        <Icon name='trash'></Icon>
                                    </Button>
                                </Item>
                                <Item>
                                    <Label>Point of Loss:</Label>
                                    <Input onChangeText={(text) => this.changeInputState("point_loss", text)} value={this.state.callReport.point_loss} />
                                </Item>
                                <Item>
                                    <Label>Date Completed:</Label>
                                    <DatePicker
                                        customStyles={{
                                            dateIcon: {
                                                display: 'none'
                                            },
                                            dateInput: {
                                                borderWidth: 0
                                            }
                                        }}
                                        mode="date"
                                        date={this.state.callReport.date_completed}
                                        placeholder="select date"
                                        format="MM/DD/YYYY"
                                        onDateChange={(date) => this.changeDateState("date_completed", date)}
                                    />
                                    <Button transparent onPress={() => this.changeDateState("date_completed", null)}>
                                        <Icon name='trash'></Icon>
                                    </Button>
                                </Item>

                                <Row>
                                    <Col>
                                        <ListItem style={styles.listView}>
                                            <CheckBox checked={this.state.callReport.is_water == '1'} onPress={() => { this.changeSelectState('is_water') }} />
                                            <Text style={[styles.defaultText]}>
                                                Water
                                            </Text>
                                        </ListItem>
                                    </Col>
                                    <Col>
                                        <ListItem style={styles.listView}>
                                            <CheckBox checked={this.state.callReport.is_sewage == '1'} onPress={() => { this.changeSelectState('is_sewage') }} />
                                            <Text style={[styles.defaultText]}>
                                                Sewage
                                            </Text>
                                        </ListItem>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <ListItem style={styles.listView}>
                                            <CheckBox checked={this.state.callReport.is_mold == '1'} onPress={() => { this.changeSelectState('is_mold') }} />
                                            <Text style={[styles.defaultText]}>
                                                Mold
                                            </Text>
                                        </ListItem>
                                    </Col>
                                    <Col>
                                        <ListItem style={styles.listView}>
                                            <CheckBox checked={this.state.callReport.is_fire == '1'} onPress={() => { this.changeSelectState('is_fire') }} />
                                            <Text style={[styles.defaultText]}>
                                                Fire
                                            </Text>
                                        </ListItem>
                                    </Col>
                                </Row>
                                <Item>
                                    <Label>Category:</Label>
                                    <Input onChangeText={(text) => this.changeInputState("category", text)} value={this.state.callReport.category} />
                                </Item>
                                <Item>
                                    <Label>Class:</Label>
                                    <Input onChangeText={(text) => this.changeInputState("class", text)} value={this.state.callReport.class} />
                                </Item>
                                <Item>
                                    <Label>Job Address:</Label>
                                    <Input onChangeText={(text) => this.changeInputState("job_address", text)} value={this.state.callReport.job_address} />
                                </Item>
                                <Item>
                                    <Label>City:</Label>
                                    <Input onChangeText={(text) => this.changeInputState("city", text)} value={this.state.callReport.city} />
                                </Item>
                                <Item>
                                    <Label>State:</Label>
                                    <Picker
                                        mode="dropdown"
                                        iosIcon={<Icon name="ios-arrow-down-outline" />}
                                        placeholder="Select State"
                                        placeholderStyle={{ color: "#bfc6ea" }}
                                        placeholderIconColor="#007aff"
                                        selectedValue={this.state.callReport.state}
                                        onValueChange={(value) => this.changeInputState("state", value)}
                                    >
                                        <Picker.Item label="-- Please select --" value="" />
                                        {statesPickerItems}
                                    </Picker>
                                </Item>
                                <Item>
                                    <Label>Zip Code:</Label>
                                    <Input onChangeText={(text) => this.changeInputState("zip_code", text)} value={this.state.callReport.zip_code} />
                                </Item>
                                <Item>
                                    <Label>Cross Streets:</Label>
                                    <Input onChangeText={(text) => this.changeInputState("cross_streets", text)} value={this.state.callReport.cross_streets} />
                                </Item>
                                <Item>
                                    <Label>Apartment Name:</Label>
                                    <Input onChangeText={(text) => this.changeInputState("apartment_name", text)} value={this.state.callReport.apartment_name} />
                                </Item>
                                <Item>
                                    <Label>Building #:</Label>
                                    <Input onChangeText={(text) => this.changeInputState("building_no", text)} value={this.state.callReport.building_no} />
                                </Item>
                                <Item>
                                    <Label>Unit #:</Label>
                                    <Input onChangeText={(text) => this.changeInputState("apartment_no", text)} value={this.state.callReport.apartment_no} />
                                </Item>
                                <Item>
                                    <Label>Gate Code:</Label>
                                    <Input onChangeText={(text) => this.changeInputState("gate_code", text)} value={this.state.callReport.gate_code} />
                                </Item>
                                <Item>
                                    <Label>Assigned to:</Label>
                                    <Picker
                                        mode="dropdown"
                                        iosIcon={<Icon name="ios-arrow-down-outline" />}
                                        placeholder="Select Team"
                                        placeholderStyle={{ color: "#bfc6ea" }}
                                        placeholderIconColor="#007aff"
                                        selectedValue={this.state.callReport.assigned_to}
                                        onValueChange={(value) => this.changeInputState("assigned_to", value)}
                                    >
                                        <Picker.Item label="-- Please select --" value="" />
                                        {teamsPickerItems}
                                    </Picker>
                                </Item>
                            </Col>
                            <Col>
                                <Text style={[styles.defaultText, styles.sectionHeaderText]}>Owner/Insured Information</Text>
                                <ListItem style={styles.listView}>
                                    <Text style={[styles.defaultText]}>
                                        Owner/Insured Name:
                                    </Text>
                                    <CheckBox checked={this.state.sameContactName} onPress={() => { this.changeSelectState('sameContactName', true) }} />
                                    <Text style={[styles.defaultText]}>
                                        Same as contact name
                                    </Text>
                                </ListItem>
                                <Item>
                                    <Input onChangeText={(text) => this.changeInputState("insured_name", text)} value={this.state.callReport.insured_name} />
                                </Item>
                                <ListItem style={styles.listView}>
                                    <Text style={[styles.defaultText]}>
                                        Billing Address:
                                    </Text>
                                    <CheckBox checked={this.state.sameJobAddress} onPress={() => { this.changeSelectState('sameJobAddress', true) }} />
                                </ListItem>
                                <Item>
                                    <Input onChangeText={(text) => this.changeInputState("billing_address", text)} value={this.state.callReport.billing_address} />
                                </Item>
                                <Item>
                                    <Label>City:</Label>
                                    <Input onChangeText={(text) => this.changeInputState("insured_city", text)} value={this.state.callReport.insured_city} />
                                </Item>
                                <Item>
                                    <Label>State:</Label>
                                    <Picker
                                        mode="dropdown"
                                        iosIcon={<Icon name="ios-arrow-down-outline" />}
                                        placeholder="Select State"
                                        placeholderStyle={{ color: "#bfc6ea" }}
                                        placeholderIconColor="#007aff"
                                        selectedValue={this.state.callReport.insured_state}
                                        onValueChange={(value) => this.changeInputState("insured_state", value)}
                                    >
                                        <Picker.Item label="-- Please select --" value="" />
                                        {statesPickerItems}
                                    </Picker>
                                </Item>
                                <Item>
                                    <Label>Zip Code:</Label>
                                    <Input onChangeText={(text) => this.changeInputState("insured_zip_code", text)} value={this.state.callReport.insured_zip_code} />
                                </Item>
                                <Item>
                                    <Label>Home Phone:</Label>
                                    <Input onChangeText={(text) => this.changeInputState("insured_home_phone", text)} value={this.state.callReport.insured_home_phone} placeholder="(702) 555-1212" />
                                </Item>
                                <Item>
                                    <Label>Cell Phone:</Label>
                                    <Input onChangeText={(text) => this.changeInputState("insured_cell_phone", text)} value={this.state.callReport.insured_cell_phone} placeholder="(702) 555-1212" />
                                </Item>
                                <Item>
                                    <Label>Work Phone:</Label>
                                    <Input onChangeText={(text) => this.changeInputState("insured_work_phone", text)} value={this.state.callReport.insured_work_phone} placeholder="(702) 555-1212" />
                                </Item>
                                <Item>
                                    <Label>Email:</Label>
                                    <Input onChangeText={(text) => this.changeInputState("insured_email", text)} value={this.state.callReport.insured_email} />
                                </Item>
                                <Item>
                                    <Label>Fax:</Label>
                                    <Input onChangeText={(text) => this.changeInputState("insured_fax", text)} value={this.state.callReport.insured_fax} placeholder="(702) 555-1212" />
                                </Item>

                                <Text style={[styles.defaultText, styles.sectionHeaderText]}>Insurance Information</Text>
                                <Item>
                                    <Label>Claim #:</Label>
                                    <Input onChangeText={(text) => this.changeInputState("insurance_claim_no", text)} value={this.state.callReport.insurance_claim_no} />
                                </Item>
                                <Item>
                                    <Label>Insurance Company:</Label>
                                    <Input onChangeText={(text) => this.changeInputState("insurance_company", text)} value={this.state.callReport.insurance_company} />
                                </Item>
                                <Item>
                                    <Label>Policy #:</Label>
                                    <Input onChangeText={(text) => this.changeInputState("insurance_policy_no", text)} value={this.state.callReport.insurance_policy_no} />
                                </Item>
                                <Item>
                                    <Label>Deductible:</Label>
                                    <Input onChangeText={(text) => this.changeInputState("insurance_deductible", text)} value={this.state.callReport.insurance_deductible} />
                                </Item>
                                <Item>
                                    <Label>Insurance Adjuster:</Label>
                                    <Input onChangeText={(text) => this.changeInputState("insurance_adjuster", text)} value={this.state.callReport.insurance_adjuster} />
                                </Item>
                                <Item>
                                    <Label>Address:</Label>
                                    <Input onChangeText={(text) => this.changeInputState("insurance_address", text)} value={this.state.callReport.insurance_address} />
                                </Item>
                                <Item>
                                    <Label>City:</Label>
                                    <Input onChangeText={(text) => this.changeInputState("insurance_city", text)} value={this.state.callReport.insurance_city} />
                                </Item>
                                <Item>
                                    <Label>State:</Label>
                                    <Picker
                                        mode="dropdown"
                                        iosIcon={<Icon name="ios-arrow-down-outline" />}
                                        placeholder="Select State"
                                        placeholderStyle={{ color: "#bfc6ea" }}
                                        placeholderIconColor="#007aff"
                                        selectedValue={this.state.callReport.insurance_state}
                                        onValueChange={(value) => this.changeInputState("insurance_state", value)}
                                    >
                                        <Picker.Item label="-- Please select --" value="" />
                                        {statesPickerItems}
                                    </Picker>
                                </Item>
                                <Item>
                                    <Label>Zip Code:</Label>
                                    <Input onChangeText={(text) => this.changeInputState("insurance_zip_code", text)} value={this.state.callReport.insurance_zip_code} />
                                </Item>
                                <Item>
                                    <Label>Work Phone:</Label>
                                    <Input onChangeText={(text) => this.changeInputState("insurance_work_phone", text)} value={this.state.callReport.insurance_work_phone} placeholder="(702) 555-1212" />
                                </Item>
                                <Item>
                                    <Label>Cell Phone:</Label>
                                    <Input onChangeText={(text) => this.changeInputState("insurance_cell_phone", text)} value={this.state.callReport.insurance_cell_phone} placeholder="(702) 555-1212" />
                                </Item>
                                <Item>
                                    <Label>Email:</Label>
                                    <Input onChangeText={(text) => this.changeInputState("insurance_email", text)} value={this.state.callReport.insurance_email} />
                                </Item>
                                <Item>
                                    <Label>Fax:</Label>
                                    <Input onChangeText={(text) => this.changeInputState("insurance_fax", text)} value={this.state.callReport.insurance_fax} placeholder="(702) 555-1212" />
                                </Item>
                            </Col>
                        </Grid>
                    </Content>
                ) : <LoadingComponent />}
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user.user,
        company: state.user.company
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        changeHeaderTitle: (headerTitle) => {
            dispatch(changeHeaderTitle(headerTitle))
        },
        visableSideBar: () => {
            dispatch(changeSideBarInivisble(false))
        },
        fetchUser: () => {
            dispatch(fetchUser())
        },
        fetchFormsOrder: () => {
            dispatch(fetchFormsOrder())
        },
        setCompanyDetails: (companyDetails) => {
            dispatch(setCompanyDetails(companyDetails))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FormCallReport);