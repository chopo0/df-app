import ApiFormsOrder from '../api/forms_order'
import {
    logoutRequest
} from '../actions/auth'

import {
    setProjectSidebar
} from '../actions/project'
import {
    changeSideBarMenus
} from '../actions/shared'

import {
    AsyncStorage
} from "react-native"

export function fetchProjectSidebar(projectId) {
    return dispatch => {
        return ApiFormsOrder.index({
                type: 'project',
                project_id: projectId
            })
            .then(response => {
                dispatch(setProjectSidebar(projectId, response.data))
                let sideBarMenus = ['<-', 'Forms']
                response.data.forEach(form => {
                    sideBarMenus.push(form.name)
                });
                dispatch(changeSideBarMenus(sideBarMenus))
            })
            .catch(response => {
                dispatch(logoutRequest())
            })
    }
}