import React from 'react';
import {
    connect
} from 'react-redux';
import {
    Content,
    Button,
    Item,
    Input,
    Form,
    Text,
    Toast,
    CheckBox,
    Label
} from "native-base";
import {
    View
} from 'react-native';
import shallowCompare from 'react-addons-shallow-compare';

import apiCategories from '../../../../../api/categories';
import BaseComponent from '../../../BaseComponent'
import {
    changeHeaderTitle,
    changeSideBarInivisble
} from '../../../../../actions/shared'
import {
    fetchUser
} from '../../../../../service/fetch_user'
import {
    handleErrorResponse
} from '../../../../../service/error_handler'
import NavigationService from '../../../../../service/NavigationService';
import styles from "../styles"

class CategoriesEdit extends BaseComponent {
    constructor(props) {
        super(props)
        this.state = {
            companyId: '',
            id: null,
            name: null,
            prefix: null
        }
    }

    componentDidMount() {
        this.subs = [
            this.props.navigation.addListener("didFocus", () => {
                this.props.invisableSideBar();
                let params = this.props.navigation.state.params                
                this.props.fetchUser()
                if (params.action === 'edit') {
                    this.setState({
                        id: params.id
                    }, () => this.initData())
                    this.props.changeHeaderTitle('Edit Category');
                } else {
                    this.setState({
                        id: null,
                        name: null,
                        prefix: null
                    })
                    this.props.changeHeaderTitle('Add Category');
                    this.loaded();
                }
                console.log("edit category page mounted")
            }),
            this.props.navigation.addListener("willBlur", () => { })
        ];
    }

    componentWillUnmount() {
        this.subs.forEach(sub => sub.remove());
    }
    
    shouldComponentUpdate(nextProps, nextState) {
        if (shallowCompare(this, nextProps, nextState)) {
            if (nextProps.company.id && !this.state.companyId) {
                nextState.companyId = nextProps.company.id
            }
            return true
        } else {
            return false;
        }
    }

    initData() {
        apiCategories.show(this.state.id)
            .then(response => {
                this.setState({
                    id: response.data.id,
                    name: response.data.name,
                    prefix: response.data.prefix
                })
                this.loaded();
            })
            .catch(handleErrorResponse)
    }

    submit() {
        this.run()
        if (this.state.id) {
            apiCategories.patch(this.state.id, {
                company_id: this.state.companyId,
                id: this.state.id,
                name: this.state.name,
                prefix: this.state.prefix
            })
                .then((response) => {
                    Toast.show({
                        text: 'Success',
                        duration: 3000,
                        type: "success"
                    });
                    this.dataReady();
                    NavigationService.goBack();
                })
                .catch(error => {
                    handleErrorResponse(error)
                    this.dataFailed()
                })
        } else {
            apiCategories.store({
                company_id: this.state.companyId,
                name: this.state.name,
                prefix: this.state.prefix
            })
                .then(response => {
                    Toast.show({
                        text: 'Success',
                        duration: 3000,
                        type: "success"
                    });
                    this.dataReady();
                    NavigationService.goBack();
                })
                .catch(error => {
                    handleErrorResponse(error)
                    this.dataFailed()
                })
        }
    }

    render() {
        return (
            this.isLoaded() ? (
                <Content padder>
                    <Form>
                        <Item>
                            <Label style={{ fontWeight: 'bold' }}>Name:</Label>
                            <Input
                                onChangeText={(text) => this.setState({
                                    name: text
                                })}
                                value={this.state.name}
                                autoCapitalize={'sentences'}
                            />
                        </Item>
                        <Item>
                            <Label style={{ fontWeight: 'bold' }}>Prefix:</Label>
                            <Input
                                onChangeText={(text) => this.setState({
                                    prefix: text
                                })}
                                value={this.state.prefix}
                                autoCapitalize={'sentences'}
                            />
                        </Item>
                    </Form>
                    <Button block info style={{ margin: 15, marginTop: 50 }}
                        onPress={() => this.submit()} disabled={this.state.isRunning}
                    >
                        <Text>
                            Ok
                        </Text>
                    </Button>
                </Content>
            ) : null
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user.user,
        company: state.user.company
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        changeHeaderTitle: (headerTitle) => {
            dispatch(changeHeaderTitle(headerTitle))
        },
        invisableSideBar: () => {
            dispatch(changeSideBarInivisble(true))
        },
        fetchUser: () => {
            dispatch(fetchUser())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CategoriesEdit);