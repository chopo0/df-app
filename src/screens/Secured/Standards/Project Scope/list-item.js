import React, { Component } from 'react';
import {
    Animated,
    Easing,
    Platform,
    Dimensions,
    Alert
} from 'react-native'
import {
    Container,
    Content,
    List,
    ListItem,
    Item,
    Button,
    Text,
    Toast,
    CheckBox,
    Body,
    Form,
    Input,
    Fab,
    Label,
    Icon,
    Row,
    Col,
    Picker,
    SwipeRow
} from "native-base";
import _ from 'lodash'
import EventEmitter from 'EventEmitter';

import {
    changeHeaderTitle,
    changeSideBarInivisble
} from '../../../../actions/shared'
import {
    handleErrorResponse
} from '../../../../service/error_handler'
import BaseComponent from '../../BaseComponent'
import LoadingComponent from '../../BaseComponent/loading'
import LegalDisclaimer from '../legal_disclaimer'
import NavigationService from '../../../../service/NavigationService';

import { fetchFormsOrder } from '../../../../service/standard_form'
import apiStandardForm from '../../../../api/standard_form'
import apiStandardScope from '../../../../api/standard_scope'
import apiUom from '../../../../api/uom'
import styles from "./styles"

export default class ScopeListItem extends Component {
    constructor(props) {
        super(props)

        this.state = {
            rowWidth: Dimensions.get('window').width * 0.94 - 50,
            item: {}
        }

        this._active = new Animated.Value(0);

        this._style = {
            ...Platform.select({
                ios: {
                    transform: [{
                        scale: this._active.interpolate({
                            inputRange: [0, 1],
                            outputRange: [1, 1.1],
                        }),
                    }],
                    shadowRadius: this._active.interpolate({
                        inputRange: [0, 1],
                        outputRange: [2, 10],
                    }),
                },

                android: {
                    transform: [{
                        scale: this._active.interpolate({
                            inputRange: [0, 1],
                            outputRange: [1, 1.07],
                        }),
                    }],
                    elevation: this._active.interpolate({
                        inputRange: [0, 1],
                        outputRange: [2, 6],
                    }),
                },
            })
        };
    }

    onLayout(e) {
        const { width, height } = Dimensions.get('window')
        this.setState({
            rowWidth: width * 0.94 - 50
        })
    }

    componentDidMount() {
        this.setState({
            item: this.props.item
        })
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.active !== nextProps.active) {
            Animated.timing(this._active, {
                duration: 300,
                easing: Easing.bounce,
                toValue: Number(nextProps.active),
            }).start();
        }
        // this.setState({
        //     item: nextProps.item
        // })
    }

    updateScopeData(item, type) {
        if (item && type === 'selected') {
            if (item.selected === 'RH') {
                item.selected = 'AH'
            } else if (item.selected === 'AH') {
                item.selected = 'X'
            } else if (item.selected === 'X') {
                item.selected = null
            } else {
                item.selected = 'RH'
            }
        }
        this.setState({
            item: item
        })
        this.props.updateScopeData(item)
    }

    setHeader(item) {
        item.header = !item.header
        this.removeItem(item)
    }

    removeItem(item) {
        item.service = null
        item.quantity = null
        item.selected = null
        item.uom = null
        this.updateScopeData(item)
    }

    render() {
        const item = this.state.item
        let uomPickerItems = this.props.uoms && this.props.uoms.map((u, i) => {
            return <Picker.Item key={i} value={u.id} label={u.title} />
        })

        return (
            <Animated.View
                onLayout={this.onLayout.bind(this)}
                style={[
                    styles.row,
                    this._style,
                ]}>
                <SwipeRow
                    style={{
                        height: 38,
                        backgroundColor: '#f7f7f7',
                        width: this.state.rowWidth,
                        marginRight: 0,
                        paddingHorizontal: 0
                    }}
                    leftOpenValue={100}
                    left={
                        <Row>
                            <Col>
                                <Button style={[]} info onPress={() => this.setHeader(item)}>
                                    <Text style={{ fontSize: 20 }}>H</Text>
                                </Button>
                            </Col>
                            <Col>
                                <Button style={[]} danger onPress={() => this.removeItem(item)}>
                                    <Icon active name="trash" />
                                </Button>
                            </Col>
                        </Row>
                    }
                    body={
                        <Row style={{ backgroundColor: item.header ? "#cccccc" : "white", paddingHorizontal: 0, width: '100%' }}>
                            <Col style={[
                                { flex: 1.5 }, styles.rightBorderCol, styles.colPadAdj
                            ]}>
                                {!item.header ? (
                                    <Button transparent dark
                                        style={[styles.defaultBtn]}
                                        onPress={() => { this.updateScopeData(item, 'selected') }}
                                    >
                                        <Text style={[styles.defaultText]}>
                                            {item.selected || ''}
                                        </Text>
                                    </Button>
                                ) : (
                                        <Text style={[{ textAlign: 'center' }, styles.headerFont]}>
                                            <Icon name="md-close" style={{ height: 28 }} />
                                        </Text>
                                    )}
                            </Col>
                            <Col style={[
                                { flex: 16 }, styles.rightBorderCol, styles.colPadAdj
                            ]}>
                                <Input
                                    style={[styles.defaultInput, item.header ? styles.headerFont : {}]}
                                    onChangeText={(text) => { this.updateScopeData({ ...this.state.item, ...{ service: text } }) }}
                                    value={item.service}
                                    autoCapitalize={'sentences'}
                                />
                            </Col>
                            <Col style={[
                                { flex: 4 }, styles.rightBorderCol, styles.colPadAdj
                            ]}>
                                {!item.header ? (
                                    <Picker
                                        mode="dropdown"
                                        iosIcon={<Icon name="ios-arrow-down-outline" />}
                                        placeholder="Select UOM"
                                        placeholderStyle={{ color: "#bfc6ea" }}
                                        placeholderIconColor="#007aff"
                                        style={[styles.defaultPicker]}
                                        selectedValue={item.uom}
                                        onValueChange={(value) => { this.updateScopeData({ ...this.state.item, ...{ uom: value } }) }}
                                    >
                                        {uomPickerItems}
                                    </Picker>
                                ) : (
                                        <Text
                                            style={[styles.defaultText, styles.headerFont]}>
                                            UOM
                                        </Text>
                                    )}

                            </Col>
                            <Col style={[
                                { flex: 4 }, styles.colPadAdj
                            ]}>
                                {!item.header ? (
                                    <Input
                                        style={[styles.defaultInput]}
                                        onChangeText={(text) => { this.updateScopeData({ ...this.state.item, ...{ quantity: text } }) }}
                                        value={item.quantity}
                                        autoCapitalize={'sentences'}
                                    />
                                ) : (
                                        <Text
                                            style={[styles.defaultText, styles.headerFont]}>
                                            QTY
                                        </Text>
                                    )}
                            </Col>
                        </Row>
                    }
                />
                <Row style={{ width: 50, backgroundColor: '#f7f7f7' }}>
                    <Col>
                        <Text><Icon active name="hand" /></Text>
                    </Col>
                </Row>
            </Animated.View >
        )
    }
}