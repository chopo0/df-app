import React from 'react';
var RN = require("react-native");
import { connect } from 'react-redux';
import {
    Content, Item, Form, Picker, Text, Button, Icon, Input, Label, Toast
} from 'native-base';
import _ from 'lodash'
import moment from 'moment'

import apiCategories from '../../../../api/categories';
import apiEquipments from '../../../../api/equipment';
import apiModels from '../../../../api/models';
import apiEquipmentLocations from '../../../../api/equipment-locations'
import { changeHeaderTitle, changeSideBarInivisble } from '../../../../actions/shared';
import BaseComponent from '../../BaseComponent';
import LoadingComponent from '../../BaseComponent/loading'

import {
    handleErrorResponse
} from '../../../../service/error_handler'


class EditEquipment extends BaseComponent {
    constructor(props) {
        super(props);
        this.state = {
            isLoaded: false,
            locations: [],
            newLocation: null,
            equipment: {
                id: null,
                category_id: null,
                model_id: null,
                team_id: null,
                status_id: null,
                company_id: null,
                serial: {
                    original: '',
                    prefix: '',
                    number: '',
                    validate: true
                },
                location: ''
            },
            categories: null,
            models: null,
            statuses: [],
            teams: [],
        };

        this.validateSerialNumber = _.debounce(this.validateSerialNumber.bind(this), 300);
        this.changedCategory = this.changedCategory.bind(this)
        this.setLocation = this.setLocation.bind(this)
        this.validateSerialNumberIme = this.validateSerialNumberIme.bind(this)
        this.changeEquipmentNumber = this.changeEquipmentNumber.bind(this)
        this.update = this.update.bind(this)
    }

    componentDidMount() {
        this.subs = [
            this.props.navigation.addListener("didFocus", () => {
                let params = this.props.navigation.state.params
                this.setState((preState) => ({
                    teams: params.teams,
                    statuses: params.statuses,
                    equipment: Object.assign({}, preState.equipment, {
                        id: params.id,
                        category_id: params.category_id
                    })
                }), () => {
                    this.initData()
                })
                this.props.invisableSideBar();
                this.props.changeHeaderTitle("Edit Equipment")
                console.log("Edit equipment mounted");
            })
        ];
    }

    componentWillUnmount() {
        this.subs.forEach(sub => sub.remove());
    }

    initData() {
        this.setState({
            isLoaded: false
        }, () => {
            const data = [
                apiCategories.index(),
                apiEquipments.show(this.state.equipment.id),
                apiModels.index({ category_id: this.state.equipment.category_id }),
                apiEquipmentLocations.index()
            ]
            return Promise.all(data)
                .then(response => {
                    let number = response[1].data.serial.replace(response[1].data.model.category.prefix + ' ', '')
                    let equipment = {
                        id: response[1].data.id,
                        category_id: response[1].data.model.category_id,
                        model_id: response[1].data.model_id,
                        team_id: response[1].data.team_id,
                        status_id: response[1].data.status_id,
                        serial: {
                            original: response[1].data.serial,
                            prefix: response[1].data.model.category.prefix,
                            number: number,
                            validate: true
                        },
                        company_id: response[1].data.company_id,
                        location: response[1].data.location,
                        project_id: response[1].data.project_id
                    }
                    let newLocation = ""
                    if (equipment.status_id !== 4) {
                        newLocation = equipment.project_id
                    }
                    this.setState(() => ({
                        categories: response[0].data.data,
                        equipment: equipment,
                        models: response[2].data.data,
                        locations: response[3].data,
                        newLocation: newLocation
                    }))
                    this.dataReady()
                    this.loaded()
                })
                .catch(handleErrorResponse)
        })
    }

    changedCategory() {
        if (!this.state.equipment.category_id) {
            this.setState((preState) => ({
                equipment: Object.assign({}, preState.equipment, {
                    model_id: null
                }),
                models: []
            }))
        } else {
            let category = this.state.categories.filter((obj) => {
                return obj.id === this.state.equipment.category_id
            })
            this.setState((preState) => ({
                equipment: Object.assign({}, preState.equipment, {
                    serial: Object.assign({}, preState.equipment.serial, {
                        prefix: category[0].prefix
                    })
                }),
                models: []
            }), () => {
                this.validateSerialNumberIme()
                apiModels.index({ category_id: this.state.equipment.category_id, show_all: true })
                    .then(response => {
                        this.setState((preState) => ({
                            equipment: Object.assign({}, preState.equipment, {
                                model_id: null
                            }),
                            models: response.data.data
                        }))
                    })
            })
        }
    }

    setLocation(location) {
        this.setState({
            newLocation: location
        }, () => {
            this.run()
            let equipment = this.state.equipment, project_id = this.state.newLocation
            let location = _.find(this.state.locations, function (obj) {
                return obj.id === project_id;
            });
            let data = {
                id: equipment.id,
                location: location.location,
                project_id: location.id,
                company_id: equipment.company_id,
                date: moment().format('YYYY-MM-DD')
            }
            apiEquipments.patch(equipment.id, data)
                .then(() => {
                        Toast.show({
                            text: 'Location successfully updated',
                            duration: 3000,
                            type: "success"
                        });
                        this.dataReady();
                    })
                .catch(handleErrorResponse)
        })
    }

    changeEquipmentNumber(number) {
        this.setState((preState) => ({
            equipment: Object.assign({}, preState.equipment, {
                serial: Object.assign({}, preState.equipment.serial, {
                    number: number
                })
            })
        }), () => { this.validateSerialNumber() })
    }

    validateSerialNumber() {
        this.validateSerialNumberIme()
    }

    validateSerialNumberIme() {
        let equipment = this.state.equipment
        if (!equipment.serial.number) {
            equipment.serial.validate = false
            this.resetEquipment(equipment)
            return false
        }
        if (equipment.serial.original === equipment.serial.prefix + ' ' + equipment.serial.number) {
            equipment.serial.validate = true
            this.resetEquipment(equipment)
            return true
        }
        let serial = equipment.serial.number
        return apiEquipments.valdiateSerial(serial, equipment.category_id)
            .then(response => {
                if (response.data.message === 'nonexistence') {
                    equipment.serial.validate = true
                    this.resetEquipment(equipment)
                    return true;
                }
                equipment.serial.validate = false
                this.resetEquipment(equipment)
                return false
            }).catch(err => {
                if (err) {
                    equipment.serial.validate = false
                    this.resetEquipment(equipment)
                    return false
                }
            })
    }

    resetEquipment(equipment) {
        this.setState((preState) => ({
            equipment: Object.assign({}, preState.equipment, {
                validate: equipment.serial.validate
            })
        }))
    }

    async update() {
        let isSerialNumberValidate = await this.validateSerialNumberIme(this.state.equipment)
        if (!isSerialNumberValidate) {
            return;
        }
        if (!this.state.equipment.category_id || !this.state.equipment.model_id || !this.state.equipment.status_id) {
            return
        }

        let data = {
            category_id: this.state.equipment.category_id,
            model_id: this.state.equipment.model_id,
            id: this.state.equipment.id,
            status_id: this.state.equipment.status_id,
            team_id: this.state.equipment.team_id,
            company_id: this.state.equipment.company_id,
            date: moment().format('YYYY-MM-DD')
        }
        if (data.status_id === 4) {
            data.location = this.state.equipment.location
        } else {
            let location = _.find(this.state.locations, (obj) => {
                return obj.id === this.state.newLocation;
            });
            data.location = location.location
            data.project_id = location.id
        }
        if (this.state.equipment.serial.original !== this.state.equipment.serial.prefix + ' ' + this.state.equipment.serial.number) {
            data.serial = this.state.equipment.serial.number.replace(new RegExp('^[0]+'), '')
        }
        this.run()
        apiEquipments.patch(this.state.equipment.id, data)
            .then(() => {
                    Toast.show({
                        text: 'Equipment successfully updated',
                        duration: 3000,
                        type: "success"
                    });
                    this.dataReady();
                })
            .catch(error => {
                handleErrorResponse(error)
                this.dataFailed()
            })
    }
    render() {
        let categoriesPickerItems = this.state.categories && this.state.categories.map((c, i) => {
            return <Picker.Item key={i} value={c.id} label={c.name} />
        })
        let modelsPickerItems = this.state.models && this.state.models.map((m, i) => {
            return <Picker.Item key={i} value={m.id} label={m.name} />
        })
        let teamsPickerItems = this.state.teams && this.state.teams.map((t, i) => {
            return <Picker.Item key={i} value={t.id} label={t.name} />
        })
        let statusesPickerItems = this.state.statuses && this.state.statuses.map((s, i) => {
            return <Picker.Item key={i} value={s.value} label={s.text} />
        })
        let locationsPickerItems = this.state.locations && this.state.locations.map((l, i) => {
            return <Picker.Item key={i} value={l.id} label={l.location} />
        })
        return (
            <Content padder>
                {this.isLoaded() ? (
                    <Form>
                        <Item error={this.state.equipment.category_id ? false : true}>
                            <Label style={{ fontWeight: 'bold' }}>Category:</Label>
                            <Picker
                                mode="dropdown"
                                iosIcon={<Icon name="ios-arrow-down-outline" />}
                                placeholder="Select Category"
                                placeholderStyle={{ color: "#bfc6ea" }}
                                placeholderIconColor="#007aff"
                                style={[{}]}
                                selectedValue={this.state.equipment.category_id}
                                onValueChange={(value) =>
                                    this.setState((preState) => ({
                                        equipment: Object.assign({}, preState.equipment, {
                                            category_id: value
                                        })
                                    }), () => {
                                        this.changedCategory()
                                    })
                                }
                            >
                                {categoriesPickerItems}
                            </Picker>
                        </Item>
                        <Item error={this.state.equipment.model_id ? false : true}>
                            <Label style={{ fontWeight: 'bold' }}>Make/Model:</Label>
                            <Picker
                                mode="dropdown"
                                iosIcon={<Icon name="ios-arrow-down-outline" />}
                                placeholder="Select Model"
                                placeholderStyle={{ color: "#bfc6ea" }}
                                placeholderIconColor="#007aff"
                                style={[{}]}
                                selectedValue={this.state.equipment.model_id}
                                onValueChange={(value) =>
                                    this.setState((preState) => ({
                                        equipment: Object.assign({}, preState.equipment, {
                                            model_id: value
                                        })
                                    }))
                                }
                            >
                                <Picker.Item label="-- Please select ---" value="" />
                                {modelsPickerItems}
                            </Picker>
                        </Item>
                        <Item>
                            <Label style={{ fontWeight: 'bold' }}>Crew/Team:</Label>
                            <Picker
                                mode="dropdown"
                                iosIcon={<Icon name="ios-arrow-down-outline" />}
                                placeholder="Select Team"
                                placeholderStyle={{ color: "#bfc6ea" }}
                                placeholderIconColor="#007aff"
                                style={[{}]}
                                selectedValue={this.state.equipment.team_id}
                                onValueChange={(value) =>
                                    this.setState((preState) => ({
                                        equipment: Object.assign({}, preState.equipment, {
                                            team_id: value
                                        })
                                    }))
                                }
                            >
                                {teamsPickerItems}
                            </Picker>
                        </Item>
                        <Item error={this.state.equipment.serial.validate ? false : true}>
                            <Label style={{ fontWeight: 'bold' }}>Equipment# : </Label>
                            <Text style={{}} numberOfLines={1}>{this.state.equipment.serial.prefix}</Text>
                            <Input keyboardType='numeric' style={{ paddingVertical: 0 }} value={this.state.equipment.serial.number} onChangeText={(text) => {
                                this.changeEquipmentNumber(text)
                            }}></Input>
                        </Item>
                        <Item error={this.state.equipment.status_id ? false : true}>
                            <Label style={{ fontWeight: 'bold' }}>Status:</Label>
                            <Picker
                                mode="dropdown"
                                iosIcon={<Icon name="ios-arrow-down-outline" />}
                                placeholder="Select Status"
                                placeholderStyle={{ color: "#bfc6ea" }}
                                placeholderIconColor="#007aff"
                                style={[{}]}
                                selectedValue={this.state.equipment.status_id}
                                onValueChange={(value) =>
                                    this.setState((preState) => ({
                                        equipment: Object.assign({}, preState.equipment, {
                                            status_id: value
                                        })
                                    }))
                                }
                            >
                                <Picker.Item label="-- Please select ---" value="" />
                                {statusesPickerItems}
                            </Picker>
                        </Item>
                        <Item>
                            <Label style={{ fontWeight: 'bold' }}>Location:</Label>
                            {this.state.equipment.status_id === 4 ? (
                                <Input style={[{ paddingVertical: 0 }]} placeholder="Location" placeholderTextColor={"#aaa"} value={this.state.equipment.location} onChangeText={(text) =>
                                    this.setState((preState) => ({
                                        equipment: Object.assign({}, preState.equipment, {
                                            location: text
                                        })
                                    }))
                                }></Input>
                            ) : (
                                    <Picker
                                        mode="dropdown"
                                        iosIcon={<Icon name="ios-arrow-down-outline" />}
                                        placeholder="Select Location"
                                        placeholderStyle={{ color: "#bfc6ea" }}
                                        placeholderIconColor="#007aff"
                                        style={[{}]}
                                        selectedValue={this.state.newLocation}
                                        onValueChange={(value) => { this.setLocation(value) }}
                                    >
                                        {locationsPickerItems}
                                    </Picker>
                                )
                            }
                        </Item>
                        <Button block info disabled={this.state.isRunning} style={{ margin: 15, marginTop: 30 }} onPress={() => this.update()}>
                            <Text>Save</Text>
                        </Button>
                    </Form>
                ) : (
                        <LoadingComponent />
                    )
                }
            </Content>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        changeHeaderTitle: (headerTitle) => {
            dispatch(changeHeaderTitle(headerTitle))
        },
        invisableSideBar: () => {
            dispatch(changeSideBarInivisble(true))
        },
    }
}

export default connect(undefined, mapDispatchToProps)(EditEquipment);
