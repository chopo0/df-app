import React, {
    Component
} from 'react';
import {
    StackNavigator
} from 'react-navigation';
import {
    Root
} from 'native-base';
import Home from './Home';
import Categories from './Categories';
import CategoriesEdit from './Categories/modals/edit'
import Models from './Models';
import Insert from './Home/insert';
import ViewEquipments from './Home/view';
import EditEquipment from './Home/edit';
import AddEquipment from './Home/add';

import NavigationService from '../../../service/NavigationService';

export const StackNav = StackNavigator({
    "Inventory": {
        screen: Home,
        path: 'equipment/home'
    },

    "Insert": {
        screen: Insert,
        path: 'equipment/home/insert'
    },

    "ViewEquipments": {
        screen: ViewEquipments,
        path: 'equipment/home/viewequipments'
    },

    "AddEquipment": {
        screen: AddEquipment,
        path: 'equipment/home/add'
    },

    "EditEquipment": {
        screen: EditEquipment,
        path: 'equipment/home/editequipment'
    },

    "Categories": {
        screen: Categories,
        path: 'equipment/categories'
    },
    
    "CategoriesEdit": {
        screen: CategoriesEdit,
        path: 'equipment/categories-edit'
    },

    "Models": {
        screen: Models,
        path: 'equipment/models'
    }
}, {
        index: 0,
        initialRouteName: "Inventory",
        headerMode: "none"
    });


export default class Nav extends Component {

    render() {
        return (
            <Root>
                <StackNav ref={
                    navigatorRef => {
                        NavigationService.setTopLevelNavigator(navigatorRef);
                    }
                }
                />
            </Root>
        );
    }
};