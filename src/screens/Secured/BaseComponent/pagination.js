import React, {
    Component
} from 'react';
import { connect } from 'react-redux';
import {
    ListItem,
    Item,
    Button,
    Text,
    CheckBox,
    Body,
    Form,
    Input,
    Fab,
    Label,
    Icon
} from "native-base";
import {
    View,
    Alert,
    Animated,
    Easing
} from 'react-native'
import _ from 'lodash'

import {
    goPage,
    initPagination
} from '../../../actions/pagination'
import styles from "./styles"

class PaginationComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            pageNum: this.props.pageNum
        }

        // this.changePageNum = _.debounce(this.changePageNum.bind(this), 300);
    }

    componentWillReceiveProps(props) {
        const pageNum = this.props.pageNum
        if (props.pageNum !== pageNum) {
            this.setState({
                pageNum: props.pageNum
            })
        }
    }

    changePageNum(text) {
        if (text == "") {
            this.setState({
                pageNum: ""
            })
            return
        }
        let pageNum = Number(text) || 1;
        pageNum = (pageNum > this.props.pageCount) ? this.props.pageCount : pageNum
        this.props.goPage(Number(pageNum), this.props.pageCount)

        this.setState({
            pageNum: "" + pageNum
        })
    }

    render() {
        return (
            <View style={{ justifyContent: 'center', alignItems: 'center', marginVertical: 10 }}>
                <Item style={{ width: 300 }}>
                    <Button small block info style={styles.navBtn}
                        onPress={() => this.props.goPage(1, this.props.pageCount)}
                        disabled={Number(this.props.pageNum) <= 1}
                    >
                        <Text style={{ textAlign: 'center' }}> {'<<'} </Text>
                    </Button>
                    <Button small block info style={styles.navBtn}
                        onPress={() => this.props.goPage(Number(this.props.pageNum) - 1, this.props.pageCount)}
                        disabled={Number(this.props.pageNum) <= 1}
                    >
                        <Text> {'<'} </Text>
                    </Button>
                    <Input keyboardType='numeric' style={{ fontSize: 15, paddingVertical: 0, flex: 1.5, textAlign: 'center', height: 32, backgroundColor: '#28B8F6', color: 'white' }} value={this.state.pageNum} onChangeText={(text) => this.changePageNum(text)}></Input>
                    <Button small block info style={styles.navBtn}
                        onPress={() => this.props.goPage(Number(this.props.pageNum) + 1, this.props.pageCount)}
                        disabled={Number(this.props.pageNum) >= this.props.pageCount}
                    >
                        <Text> {'>'} </Text>
                    </Button>
                    <Button small block info style={styles.navBtn}
                        onPress={() => this.props.goPage(this.props.pageCount, this.props.pageCount)}
                        disabled={Number(this.props.pageNum) >= this.props.pageCount}
                    >
                        <Text> {'>>'} </Text>
                    </Button>
                </Item>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        pageNum: state.pagination.pageNum,
        pageCount: state.pagination.pageCount
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        goPage: (pageNum, pageCount) => {
            dispatch(goPage(pageNum, pageCount))
        },
        initPagination: (pageCount) => {
            dispatch(initPagination(pageCount))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PaginationComponent);