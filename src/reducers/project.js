import {
    SET_PROJECT,
    SET_PROJECT_FORMS,
    SET_PROJECT_SIDEBAR
} from '../actions/actionTypes'

const initialState = {
    projectId: null,
    projectForms: [],
    projectSidebar: [],
    callReport: {}
}

export default function project(state = initialState, action) {
    console.log(action);
    switch (action.type) {
        case SET_PROJECT:
            if (action.callReport) {
                return Object.assign({}, state, {
                    projectId: action.projectId,
                    callReport: action.callReport
                });
            } else {
                return Object.assign({}, state, {
                    projectId: action.projectId
                });
            }
        case SET_PROJECT_FORMS:
            return Object.assign({}, state, {
                projectId: action.projectId,
                projectForms: action.projectForms
            });
        case SET_PROJECT_SIDEBAR:
            return Object.assign({}, state, {
                projectId: action.projectId,
                projectSidebar: action.projectSidebar
            });
        default:
            return state;
    }
}