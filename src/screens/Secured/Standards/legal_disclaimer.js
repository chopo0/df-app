import React, {Component} from "react";
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';
import _ from 'lodash'
import { Alert } from 'react-native'
import EventEmitter from 'EventEmitter';

import BaseComponent from '../BaseComponent'
import {
    fetchUser
} from '../../../service/fetch_user'
import {
    setCompany
} from '../../../actions/user'
import apiCompanies from '../../../api/company'
import {
    handleErrorResponse
} from '../../../service/error_handler'

class LegalDisclaimer extends BaseComponent {
    constructor(props) {
        super(props)

        this.state = {
            companyId: '',
            title: 'Legal Disclaimer',
            skipDisclaimer: false
        }
    }

    componentDidMount() {
        this.setState({
            companyId: ''
        }, () => {
            this.props.fetchUser()
        })

        this.props.emitter.addListener('parent component mounted', () => {
            this.setState({
                companyId: ''
            }, () => {
                this.props.fetchUser()
            })
        });
    }
    
    shouldComponentUpdate(nextProps, nextState) {
        if (shallowCompare(this, nextProps, nextState)) {
            if (nextProps.company.id && !this.state.companyId && !nextProps
                .company.skip_disclaimer) {
                nextState.companyId = nextProps.company.id
                this.showAlert()
            }
            return true
        } else {
            return false;
        }
    }    

    showAlert() {
        Alert.alert(
            this.state.title,
            'Please Note: Any legal document(s) used or modified, should always be reviewed by an attorney in your local area.',
            [
                {text: 'Do not show this dialog again.', onPress: () => {
                    let companyData = this.props.company
                    companyData.skip_disclaimer = true
                    apiCompanies.patch(this.state.companyId, companyData)
                        .then(response => {
                        })
                        .catch(error => {
                            handleErrorResponse(error)
                        })
                }},
                {text: 'Ok', onPress: () => {
                }},
            ],
            { cancelable: false }
        )
    }
    render() {
        return (
            null
        );
    }
}
  
const mapStateToProps = (state) => {
    return {
        user: state.user.user,
        company: state.user.company,
        isSubscribed: state.user.is_subscribed,
        isGracePeriod: state.user.is_grace_period
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchUser: () => {
            dispatch(fetchUser())
        },
        setCompany: (company) => {
            dispatch(setCompany(company))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(LegalDisclaimer);