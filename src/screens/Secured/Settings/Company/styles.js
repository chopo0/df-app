var React = require("react-native");
var {
    Dimensions
} = React;

import commonColor from "../../../../theme/variables/commonColor";

export default {
    btnText: {
        marginHorizontal:10,
        marginTop: 10
    },
    chkBox: {
        flex: 0.15
    },
    permissonBox: {
        flex: 0.85
    }
};