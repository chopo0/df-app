import React, {
    Component
} from 'react';
import {
    connect
} from 'react-redux';
import {
    Content,
    Button,
    Icon,
    Text,
    List,
    Toast,
    Fab,
    CheckBox,
    ListItem,
    Body
} from "native-base";
import {
    ListView,
    View
} from 'react-native';
import { Alert } from 'react-native'
import styles from "./styles"
import {RichTextEditor, RichTextToolbar} from 'react-native-zss-rich-text-editor';
import ImagePicker from 'react-native-image-picker'
import _ from 'lodash'
import {
    changeHeaderTitle,    
    changeSideBarInivisble
} from '../../../../actions/shared'
import {
    handleErrorResponse
} from '../../../../service/error_handler'
import BaseComponent from '../../BaseComponent'
import apiProjectStatements from '../../../../api/project_statements'

const imgPickerOptions = {
    title: 'Select Image',
    storageOptions: {
        skipBackup: true,
        path: 'dryforms'
    },
    width: 150,
    height: 150,
    cropping: true,
};

class Statement extends BaseComponent {
    constructor(props) {
        super(props)
        this.state = {
            currStatement: ""
        }

        this.changeContentText = _.debounce(this.changeContentText.bind(this), 2000);
        this.uploadImage = this.uploadImage.bind(this);
    }

    componentDidMount() {
        this.setState({
            currStatement: this.props.statementInfo.statement
        })
    }

    componentWillReceiveProps(props) {
        const refresh = this.props.refresh
        if (props.refresh !== refresh) {
            this.richtext.setContentHTML(props.statementInfo.statement)
        }
    }
    
    revert() {
        Alert.alert(
            'Revert Statement',
            'Are you sure you want to revert to the default statement?',
            [
                {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                {text: 'Confirm', onPress: () => {
                    this.run()
                    apiProjectStatements.revert(this.props.statementInfo.id, {
                        form_id: this.props.formId
                    }).then(response => {
                        this.props.onRevert()
                        this.dataReady()
                    }).catch(error => {
                        handleErrorResponse(error)
                        this.dataFailed();
                    })
                }},
            ],
            { cancelable: false }
        )
    }

    changeContentText(text) {
        if (text === this.state.currStatement) return;
        this.setState({
            currStatement: text
        })
        this.props.onUpdate(this.props.statementInfo.id, text)
    }

    uploadImage() {
        ImagePicker.showImagePicker(imgPickerOptions, (response) => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                let source = 'data:image/jpeg;base64,' + response.data;
                this.richtext.insertImage({ src: source, width: 100, height: 100 });
            }
        });
    }

    getContent() {
        return this.richtext.getContentHtml();
    }

    render() {
        return (
            <View style={styles.container}>
                <RichTextEditor
                    enableOnChange={ true }
                    ref={(r)=>{
                        this.richtext = r
                        if (this.richtext) {
                            this.richtext.registerContentChangeListener(this.changeContentText);
                        }
                    }}
                    style={styles.richText}
                    hiddenTitle= {true}
                    initialContentHTML={this.props.statementInfo.statement}
                />
                <RichTextToolbar
                    getEditor={() => this.richtext}
                    onPressAddImage={()=>{
                        this.uploadImage();
                    }}
                    iconTint='black'
                />
                <Button info style={{ margin: 15, marginVertical: 10 }}
                onPress={()=> this.revert()} disabled={this.state.isRunning}
                >
                    <Text>
                        Revert
                    </Text>
                </Button>
            </View>
        )
    }
}

export default Statement;