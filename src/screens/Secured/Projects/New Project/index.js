import React from 'react';
import {
    connect
} from 'react-redux';
import { Container, Grid, Content, Item, List, ListItem, Body, Picker, Text, Button, Fab, Badge, Icon, Row, Col, Spinner, Input, Header, Thumbnail, CheckBox } from 'native-base';
import {
    ListView,
    FlatList,
    View
} from 'react-native';
import _ from 'lodash'
import { Alert } from 'react-native'
import EventEmitter from 'EventEmitter';
import shallowCompare from 'react-addons-shallow-compare';

import NavigationService from '../../../../service/NavigationService'
import {
    changeHeaderTitle,
    changeSideBarInivisble
} from '../../../../actions/shared'
import {
    handleErrorResponse
} from '../../../../service/error_handler'
import BaseComponent from '../../BaseComponent'
import styles from "./styles"
import apiFormsOrder from '../../../../api/forms_order'
import { fetchFormsOrder } from '../../../../service/standard_form'
import {
    fetchUser
} from '../../../../service/fetch_user'
import LoadingComponent from '../../BaseComponent/loading'

class NewProject extends BaseComponent {
    constructor(props) {
        super(props)

        this.state = {
            selectedForms: [],
            forms: []
        }

        this.ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
    }

    componentDidMount() {
        this.subs = [
            this.props.navigation.addListener("didFocus", () => {
                this.props.changeHeaderTitle("Select Forms for this project")
                this.props.visableSideBar()
                this.init()
                console.log("new project mounted")
            }),
            this.props.navigation.addListener("willBlur", () => { })
        ];
    }

    componentWillUnmount() {
        this.subs.forEach(sub => sub.remove());
    }

    init() {
        this.run()
        apiFormsOrder.index()
            .then(response => {
                let forms = _.filter(response.data, (form) => {
                    return _.indexOf([1, 12], form.form_id) === -1
                })
                forms.forEach((form) => {
                    form.selected = '1'
                })
                this.setState({
                    forms: forms
                }, () => {
                    this.dataReady()
                })
            })
            .catch(() => {
                handleErrorResponse(error);
                this.dataFailed();
            })
    }

    changeSelected(item) {
        let forms = []
        this.state.forms.forEach(form => {
            if (form.form_id == item.form_id) {
                form.selected = '' + (1 - Number(item.selected))
            }
            forms.push(form)
        })
        this.setState({
            forms: forms
        })
    }

    createProject() { }

    render() {
        return (
            <Container>
                {!this.state.isRunning ? (
                    <Content padder>
                        <FlatList style={{}}
                            data={this.state.forms}
                            renderItem={({ item }) => {
                                return (<ListItem style={styles.listView}>
                                    <CheckBox checked={item.selected == '1'} onPress={() => this.changeSelected(item)} />
                                    <Text style={[styles.defaultText]} numberOfLines={1}>
                                        {item.name}
                                    </Text>
                                </ListItem>)
                            }}
                        />
                        <Button block info onPress={() => { this.createProject() }}>
                            <Text>Save</Text>
                        </Button>
                    </Content>
                ) : <LoadingComponent />}
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user.user,
        company: state.user.company
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        changeHeaderTitle: (headerTitle) => {
            dispatch(changeHeaderTitle(headerTitle))
        },
        visableSideBar: () => {
            dispatch(changeSideBarInivisble(false))
        },
        fetchUser: () => {
            dispatch(fetchUser())
        },
        fetchFormsOrder: () => {
            dispatch(fetchFormsOrder())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewProject);