import axios from './interceptor';
const statusResource = '/api/statuses';

export default {
    index(data) {
        return axios.get(statusResource, data);
    }
}